# Letters Configuration

This repository is used to store backup copies and provide version control for customizations made
to Duke's Alma letter repository.

There are over 100 letters in Alma that are coded in XSLT. Not all letters are in use. 

Only store a copy of a letter in here if we have modified the default ExLibris text or label(s).

Store letter XSLT in /letters and store components XSLT in /components.

## File naming convention
When adding a file, use the name of the letter, with a '-' in place of spaces, and end in '.xlst'. 

For example, the letter in Alma named "Borrowed By Letter" would have the filename "borrowed-by-letter.xslt" in this repository.

## Backup a new letter or component

To back up a new letter or component:
* Go to the appropriate directory;
* From the (+) menu option, choose this directory > new file
* At the top, provide a filename
* Copy/paste the XSLT file from the Alma screen into the box for "New File" 
* Scroll down to "Commit message" and provide a short message about the file you are adding
* click "Commit changes" or "Open a merge request" to save your changes

## Add a new version of a letter or component already in the repository

To add a new version of a letter or component already in the repository:
* Go to the appropriate directory;
* Click the file you want to update;
* From the file screen, click "Edit" and choose "Edit single file"
* In the edit screen, copy/paste the new XSLT from Alma into the file  to replace the prior version
* Scroll down to "Commit message" and provide a short update about what is changing
* Click "Commit changes" or "Open a merge request" to save your changes
