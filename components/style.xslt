<?xml version="1.0" encoding="utf-8"?>

<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    
    <!-- FOR COMPATABILITY WITH MOBILE EMAIL APPS, STYLE MUST BE DECLARED INLINE -->
    
    <xsl:template name="generalStyle">
        <style>
            body {@@language_related_body_css@@ background-color:#fff}
            .listing td {border-bottom: 1px solid #eee}
            <!-- .listing tr:hover td {background-color:#eee} TURNING OFF HOVER FOR NOW -->
            .listing th {background-color:#f5f5f5 }
            h4{line-height: 1.25em}
        </style>
    </xsl:template>
    
    <xsl:template name="bodyStyleCss">
        font-family:Calibri; color:#333333; margin:0.2em; padding:0em; font-size:100%; width:100%;
    </xsl:template>
    
    <xsl:template name="listStyleCss">
        list-style: none; margin:0 0 0 1em; padding:0;
    </xsl:template>
    
    <xsl:template name="mainTableStyleCss">
        width:100%; text-align:center; vertical-align:center; margin-bottom:1.8em;
    </xsl:template>
    
    <xsl:template name="headerLogoStyleCss">
        background-color:#ffffff;  width:100%;
    </xsl:template>
    
    <xsl:template name="headerTableStyleCss">
        background-color:#e9e9e9;  width:100%; margin:0.2em;  font-weight:700; line-height:1em; font-size:100%; vertical-align:middle;
    </xsl:template>
    
    <!-- margin property order is Top, Right, Bottom, Left -->
    
    <xsl:template name="footerTableStyleCss">
        background-color:#00009C;  width:100%; color:#fff; margin:1 0.2 0 0.2em; font-size:5%;
    </xsl:template>
    
    
    <xsl:template name="footerTextStyleCss">
        font-family: arial; color:#333; width:100%; margin:1 0.2 0 0.2em; padding:0em; font-size:100%; line-height:2em;
    </xsl:template>
    
</xsl:stylesheet>