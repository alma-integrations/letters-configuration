<?xml version="1.0" encoding="utf-8"?>

<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    
    
    
    <xsl:template name="toWhomIsConcerned">
        <xsl:choose>
            
            <!-- CHECK THIS LOCATION FIRST -->
            <xsl:when test="notification_data/user_for_printing/first_name" >
                <!-- USE PREFERRED NAMES IF AVAILABLE, DEFAULT TO REGULAR NAMES -->
                @@dear@@
                <xsl:variable name="first" select="concat(normalize-space(notification_data/user_for_printing/preferred_first_name), ' ')" />
                <xsl:choose>
                    <xsl:when test="boolean(normalize-space(notification_data/user_for_printing/preferred_first_name))">
                        <!-- <xsl:when test="$first != ''"> -->
                        <xsl:value-of select="$first"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="concat(normalize-space(notification_data/user_for_printing/first_name), ' ')" />
                    </xsl:otherwise>
                </xsl:choose>
                
                <xsl:variable name="last" select="concat(normalize-space(notification_data/user_for_printing/preferred_last_name), ',')" />
                <xsl:choose>
                    <!-- <xsl:when test="$last != ''"> -->
                    <xsl:when test="boolean(normalize-space(notification_data/user_for_printing/preferred_last_name))">      
                        <xsl:value-of select="$last" />
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="concat(normalize-space(notification_data/user_for_printing/last_name), ',')" />
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:when>
            
            <!-- FOR LETTERS THAT DO NOT CONTAIN "first_name," "last_name," "preferred_first_name," OR "preferred_last_name" IN THE "user_for_printing" NODE -->
            <!-- (E.G. FulOverdueAndLostLoanLetter ) -->
            <xsl:when test="notification_data/receivers/receiver/user" >
                <!-- USE PREFERRED NAMES IF AVAILABLE, DEFAULT TO REGULAR NAMES -->
                @@dear@@
                <xsl:variable name="first" select="concat(normalize-space(notification_data/receivers/receiver/user/preferred_first_name), ' ')" />
                <xsl:choose>
                    <xsl:when test="boolean(normalize-space(notification_data/receivers/receiver/user/preferred_first_name))">
                        <!-- <xsl:when test="$first != ''"> -->
                        <xsl:value-of select="$first"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="concat(normalize-space(notification_data/receivers/receiver/user/first_name), ' ')" />
                    </xsl:otherwise>
                </xsl:choose>
                
                <xsl:variable name="last" select="concat(normalize-space(notification_data/receivers/receiver/user/preferred_last_name), ',')" />
                <xsl:choose>
                    <!-- <xsl:when test="$last != ''"> -->
                    <xsl:when test="boolean(normalize-space(notification_data/receivers/receiver/user/preferred_last_name))">      
                        <xsl:value-of select="$last" />
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="concat(normalize-space(notification_data/receivers/receiver/user/last_name), ',')" />
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:when>
        </xsl:choose>
    </xsl:template>
    
</xsl:stylesheet>