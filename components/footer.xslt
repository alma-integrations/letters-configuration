<?xml version="1.0" encoding="utf-8"?>

<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    
    
    <xsl:template name="salutation">        
    </xsl:template>
    
    <!-- Removed duke logo template because quality was poor. EN 572024 -->
    
    <xsl:template name="dukeLogo">
    </xsl:template> 
    
    <!-- Updated template to use correct Duke Blue and thinner line. EN 572024 -->
    
    <xsl:template name="blueLine">
        <table width="100%" padding="15px">
            <tr>
                <td height="5" bgcolor="#012169"> </td>
            </tr>
        </table>
    </xsl:template>
    
    <!-- USE THIS 'CONTACT US' FOR EVERYTHING BUT REQUESTS -->
    <xsl:template name="contactUs">
        <table align="left">
            <tr>
                <td align="left">
                    <!-- Since the letter needs to originate from the holding library, we use the organization_unit info -->
                    <a href="mailto:{notification_data/organization_unit/email/email}">Contact us</a>
                </td>
            </tr>
        </table>
    </xsl:template>
       
    <xsl:template name="myAccount">
        <table align="right">
            <tr>
                <td align="right">
                    <a>
                        <xsl:attribute name="href">
                            @@email_my_account@@
                        </xsl:attribute>
                        My Accounts
                    </a>
                </td>
            </tr>
        </table>
    </xsl:template>
    
    <!-- NOT SPECIFIC TO DUL, DESPITE THE TEMPLATE NAME -->
    <xsl:template name="footerTextDUL">
        <div align="left" style="font-size:11pt">                 
            <a href="https://library.duke.edu/my-accounts">Your Library Account</a> |
            <a href="https://library.duke.edu/using/borrowing">Borrowing Policies</a> |
            <a href="https://library.duke.edu/about/hours">Library Hours</a> |
            <a href="https://library.duke.edu/services/disabilities">Accessibility Assistance</a>
        </div>
    </xsl:template>
    
    <!-- SO THAT DEFAULT LETTERS DON'T BREAK -->
    <xsl:template name="lastFooter">
    </xsl:template>
    
</xsl:stylesheet>