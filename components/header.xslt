<?xml version="1.0" encoding="utf-8"?>

<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    
    
    
    <xsl:template name="head">
        
        <!-- JLK44 added parameter to get letter names from letter labels instead of xml fields -->
        <!-- (i.e. to make the labels actually work). If no param is supplied from letter xsl, defaults to xml letter name as orig coded -->
        <xsl:param name="letterNameFromLabels" select="notification_data/general_data/letter_name" />
        <table cellspacing="0" cellpadding="5" border="0">
            <xsl:attribute name="style">
                <xsl:call-template name="headerTableStyleCss" /> <!-- style.xsl -->
            </xsl:attribute>
            <tr>
                <xsl:for-each select="notification_data/general_data">
                    <td>
                        <h4>
                            <xsl:value-of select="$letterNameFromLabels" />
                            <!--<xsl:value-of select="letter_name"/>   -->
                        </h4>
                    </td>
                    <td align="right">
                        <xsl:value-of select="current_date"/>
                    </td>
                </xsl:for-each>
                
            </tr>
        </table>
        
        
    </xsl:template>
    
</xsl:stylesheet>