<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    
    <xsl:include href="header.xsl" />
    <xsl:include href="senderReceiver.xsl" />
    <xsl:include href="mailReason.xsl" />
    <xsl:include href="footer.xsl" />
    <xsl:include href="style.xsl" />
    <xsl:include href="recordTitle.xsl" />
    
    <xsl:template match="/">
        <html>
            <xsl:if test="notification_data/languages/string">
                <xsl:attribute name="lang">
                    <xsl:value-of select="notification_data/languages/string"/>
                </xsl:attribute>
            </xsl:if>
            
            <head>
                <title>
                    <xsl:value-of select="notification_data/general_data/subject"/>
                </title>
                
                <xsl:call-template name="generalStyle" /> <!-- style.xsl -->
            </head>
            <body>
                
                <xsl:attribute name="style">
                    <xsl:call-template name="bodyStyleCss" /> <!-- style.xsl -->
                </xsl:attribute>
                
                <!-- GET HOLDING LIB CONTACT INFO -->
                <xsl:variable name="holdingLibEmail">
                    <xsl:call-template name="holdingLibEmail"/> <!-- senderReceiver.xsl -->
                </xsl:variable>
                <xsl:variable name="holdingLibPhone">
                    <xsl:call-template name="holdingLibPhone"/> <!-- senderReceiver.xsl -->
                </xsl:variable>
                <!-- ONLY CERTAIN LIBRARY NAMES BEGIN WITH "THE" -->
                <xsl:variable name="article">
                    <xsl:choose>
                        <xsl:when test="notification_data/organization_unit/name = 'Perkins/Bostock Library' or notification_data/organization_unit/name = 'Lilly Library'">
                            <text></text>
                        </xsl:when>
                        <xsl:otherwise>
                            <text>the </text>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:variable>
                
                <table role='presentation'  cellspacing="0" cellpadding="5" border="0" style="font-family:Calibri; color:#333333; font-size:12pt; width:100%">
                    <xsl:if test="notification_data/item_loans/item_loan or notification_data/overdue_item_loans/item_loan">
                        <tr>
                            <td height="60">
                                <!-- LETTER GREETING -->
                                <xsl:call-template name="toWhomIsConcerned" /> <!-- mailReason.xsl -->
                            </td>
                        </tr>
                        
                        <tr>
                            <td height="40">
                                @@reminder_message@@                
                                <xsl:value-of select="concat('from ', $article, normalize-space(notification_data/organization_unit/name), ':')" />
                                <br />
                                <br />
                            </td>
                        </tr>
                        
                        <xsl:if test="notification_data/overdue_loans_by_library/library_loans_for_display">
                            <!-- CHECK FOR "Check for" CIRC NOTES AMONG OVERDUE LOANS -->
                            <xsl:variable name="overdueNotesCount">
                                <xsl:value-of select="count(notification_data/overdue_item_loans/item_loan/
                                    physical_item[starts-with(translate(fulfillment_note, 'C', 'c'), 'check for')])" />
                            </xsl:variable>
                            <xsl:for-each select="notification_data">
                                <tr>
                                    <td><h3>@@overdue_loans@@</h3></td>
                                </tr>
                                <tr>
                                    <td>
                                        <table caption="overdue loans" cellpadding="5">
                                            <thead style="background-color:#f5f5f5; text-align:center">
                                                <tr>
                                                    <th scope="col">@@title@@</th>
                                                    <th scope="col">@@author@@</th>
                                                    <th scope="col">@@call_number@@</th>
                                                    <th scope="col">@@description@@</th>
                                                    <th scope="col">@@due_date@@</th>
                                                    <th scope="col">Barcode</th>
                                                    <!-- ONLY SHOW THIS COLUMN IF THERE ARE NOTES TO DISPLAY -->
                                                    <xsl:if test="$overdueNotesCount &gt; 0">
                                                        <th>
                                                            Additional Pieces
                                                        </th>
                                                    </xsl:if>
                                                </tr>
                                            </thead>
                                            
                                            <xsl:for-each select="overdue_item_loans/item_loan">
                                                <tr style="text-align:left">
                                                    <td style="border-bottom: 1px solid #eee"><xsl:value-of select="title"/></td>
                                                    <td style="border-bottom: 1px solid #eee">
                                                        <!-- CHOOSE BEST AUTHOR FIELD, REMOVE AUTHORITY FILE LINK -->
                                                        <!-- (Sometimes "author" node is blank when "physical_item/author" is populated) -->
                                                        <xsl:choose>
                                                            <xsl:when test="author=''">
                                                                <xsl:value-of select="physical_item/author" />
                                                            </xsl:when>   
                                                            <xsl:otherwise>
                                                                <xsl:choose>
                                                                    <xsl:when test="contains(author, 'https://id.')">
                                                                        <xsl:value-of select="substring-before(author, ' https://id.')"/>
                                                                        <text> </text>
                                                                        <xsl:value-of select="substring-after(substring-after(author, 'https://id.'), ' ')"/>
                                                                    </xsl:when>
                                                                    <xsl:otherwise>
                                                                        <xsl:value-of select="author"/>
                                                                    </xsl:otherwise>
                                                                </xsl:choose>
                                                            </xsl:otherwise>
                                                        </xsl:choose>
                                                    </td>
                                                    <td><xsl:value-of select="call_number"/></td>
                                                    <td><xsl:value-of select="description"/></td>
                                                    <td style="border-bottom: 1px solid #eee; text-align:center; color:red; font-weight:bold;">
                                                        <!-- "due_date" CONTAINS JUST THE DATE (MM/DD/YY)-->
                                                        <xsl:value-of select="due_date"/>
                                                        <!-- ONLY DISPLAY THE DUE TIME IF IT IS NOT "23:59" -->
                                                        <!-- "new_due_date_str" CONTAINS JUST DATE (MM/DD/YY) AND TIME (HH:MM:SS) -->
                                                        <xsl:if test="not(contains(new_due_date_str, '23:59'))">
                                                            <br />
                                                            <!-- EXTRACT TIME AS (HH:MM) -->
                                                            <!-- CONVERT TO 12-HOUR CLOCK WITH "AM/PM" -->
                                                            <xsl:choose>
                                                                <!-- (ALMA USES "24" INSTEAD OF "00" FOR 12AM) -->
                                                                <!-- (BUT IT DOES *NOT* MEAN MIDNIGHT AT END OF THAT DATE) -->
                                                                <xsl:when test="number(substring(new_due_date_str, 12, 2) mod 12 = 0)">
                                                                    <xsl:value-of select="concat('12', substring(new_due_date_str, 14, 3))"/>
                                                                </xsl:when>                                                                    
                                                                <xsl:otherwise>
                                                                    <xsl:value-of select="concat(number(substring(new_due_date_str, 12, 2)) mod 12, substring(new_due_date_str, 14, 3))"/>
                                                                </xsl:otherwise>
                                                            </xsl:choose>
                                                            <xsl:choose>
                                                                <xsl:when test="substring(new_due_date_str, 12, 2) &gt;= 12 and substring(new_due_date_str, 12, 2) != 24">
                                                                    <text>PM</text>
                                                                </xsl:when>
                                                                <xsl:otherwise>
                                                                    <text>AM</text>
                                                                </xsl:otherwise>
                                                            </xsl:choose>
                                                        </xsl:if>
                                                    </td>
                                                    
                                                    <td style="border-bottom: 1px solid #eee">
                                                        <xsl:value-of select="barcode"/>
                                                    </td>
                                                    <!-- LOOK FOR CIRC NOTES BEGINNING WITH "Check for" OR "check for" -->
                                                    <xsl:if test="$overdueNotesCount &gt; 0 and starts-with(translate(physical_item/fulfillment_note, 'C', 'c'), 'check for')">
                                                        <!-- REMOVE "Check for" TO GET JUST THE EXTRA PIECES -->
                                                        <td style="border-bottom: 1px solid #eee"><xsl:value-of select="substring-after(physical_item/fulfillment_note, 'heck for')" /></td>
                                                    </xsl:if>
                                                </tr>
                                            </xsl:for-each>
                                        </table>
                                    </td>
                                </tr>
                            </xsl:for-each>
                            <tr>
                                <td height="80">
                                    <text>Please return any overdue items to a </text>
                                    <a href="https://library.duke.edu/libraries">Duke Libraries location</a>
                                    <text> as soon as possible, or </text>
                                    <a href="https://library.duke.edu/my-accounts">renew</a>
                                    <text> them if needed.</text>
                                    <br />
                                    <xsl:if test="$overdueNotesCount &gt; 0">
                                        <text>Be sure to check for any additional pieces that came with the items before returning them.</text>
                                        <br />
                                    </xsl:if>
                                    <text>Some items cannot be renewed if they have been requested by other patrons or if they are needed for Course Reserves.</text>
                                    <br/><br/>
                                </td>
                            </tr>
                        </xsl:if>
                        <br />
                        
                        <xsl:if test="notification_data/loans_by_library/library_loans_for_display">
                            <!-- CHECK FOR "Check for" CIRC NOTES AMONG REGULAR LOANS -->
                            <xsl:variable name="loanNotesCount">
                                <xsl:value-of select="count(notification_data/item_loans/item_loan/
                                    physical_item[starts-with(translate(fulfillment_note, 'C', 'c'), 'check for')])" />
                            </xsl:variable>
                            <xsl:for-each select="notification_data">
                                <tr>
                                    <td>
                                        <h3>@@loans@@</h3>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table caption="loans" role='presentation' cellpadding="5">
                                            <thead style="background-color:#f5f5f5; text-align:center">
                                                <tr>
                                                    <th scope="col">@@title@@</th>
                                                    <th scope="col">@@author@@</th>
                                                    <th scope="col">@@call_number@@</th>
                                                    <th scope="col">@@description@@</th>
                                                    <th scope="col">@@due_date@@</th>
                                                    <th scope="col">Barcode</th>
                                                    <!-- ONLY SHOW THIS COLUMN IF THERE ARE NOTES TO DISPLAY -->
                                                    <xsl:if test="$loanNotesCount &gt; 0">
                                                        <th scope="col">
                                                            Additional Pieces
                                                        </th>
                                                    </xsl:if>
                                                </tr>
                                            </thead>
                                            
                                            <xsl:for-each select="item_loans/item_loan">
                                                <tr style="text-align:left">
                                                    <td style="border-bottom: 1px solid #eee"><xsl:value-of select="title"/></td>
                                                    <td style="border-bottom: 1px solid #eee">
                                                        <!-- CHOOSE BEST AUTHOR FIELD, REMOVE AUTHORITY FILE LINK -->
                                                        <!-- (Sometimes "author" node is blank when "physical_item/author" is populated) -->
                                                        <xsl:choose>
                                                            <xsl:when test="author=''">
                                                                <xsl:value-of select="physical_item/author" />
                                                            </xsl:when>   
                                                            <xsl:otherwise>
                                                                <xsl:choose>
                                                                    <xsl:when test="contains(author, 'https://id.')">
                                                                        <xsl:value-of select="substring-before(author, ' https://id.')"/>
                                                                        <text> </text>
                                                                        <xsl:value-of select="substring-after(substring-after(author, 'https://id.'), ' ')"/>
                                                                    </xsl:when>
                                                                    <xsl:otherwise>
                                                                        <xsl:value-of select="author"/>
                                                                    </xsl:otherwise>
                                                                </xsl:choose>
                                                            </xsl:otherwise>
                                                        </xsl:choose>
                                                    </td>
                                                    <td style="border-bottom: 1px solid #eee"><xsl:value-of select="call_number"/></td>
                                                    <td style="border-bottom: 1px solid #eee"><xsl:value-of select="description"/></td>
                                                    <td style="border-bottom: 1px solid #eee; text-align:center">
                                                        <!-- "due_date" CONTAINS JUST THE DATE (MM/DD/YY)-->
                                                        <xsl:value-of select="due_date"/>
                                                        <!-- ONLY DISPLAY THE DUE TIME IF IT IS NOT "23:59" -->
                                                        <!-- "new_due_date_str" CONTAINS JUST DATE (MM/DD/YY) AND TIME (HH:MM:SS) -->
                                                        <xsl:if test="not(contains(new_due_date_str, '23:59'))">
                                                            <br />
                                                            <!-- EXTRACT TIME AS (HH:MM) -->
                                                            <!-- CONVERT TO 12-HOUR CLOCK WITH "AM/PM" -->
                                                            <xsl:choose>
                                                                <!-- (ALMA USES "24" INSTEAD OF "00" FOR 12AM) -->
                                                                <!-- (BUT IT DOES *NOT* MEAN MIDNIGHT AT END OF THAT DATE) -->
                                                                <xsl:when test="number(substring(new_due_date_str, 12, 2) mod 12 = 0)">
                                                                    <xsl:value-of select="concat('12', substring(new_due_date_str, 14, 3))"/>
                                                                </xsl:when>                                                                    
                                                                <xsl:otherwise>
                                                                    <xsl:value-of select="concat(number(substring(new_due_date_str, 12, 2)) mod 12, substring(new_due_date_str, 14, 3))"/>
                                                                </xsl:otherwise>
                                                            </xsl:choose>
                                                            <xsl:choose>
                                                                <xsl:when test="substring(new_due_date_str, 12, 2) &gt;= 12 and substring(new_due_date_str, 12, 2) != 24">
                                                                    <text>PM</text>
                                                                </xsl:when>
                                                                <xsl:otherwise>
                                                                    <text>AM</text>
                                                                </xsl:otherwise>
                                                            </xsl:choose>
                                                        </xsl:if>
                                                    </td>
                                                    <td style="border-bottom: 1px solid #eee">
                                                        <xsl:value-of select="barcode"/>
                                                    </td>
                                                    <!-- LOOK FOR CIRC NOTES BEGINNING WITH "Check for" OR "check for" -->
                                                    <xsl:if test="$loanNotesCount &gt; 0">
                                                        <xsl:choose>
                                                            <xsl:when test="starts-with(translate(physical_item/fulfillment_note, 'C', 'c'), 'check for')">
                                                                <!-- REMOVE "Check for" TO GET JUST THE EXTRA PIECES -->
                                                                <td style="border-bottom: 1px solid #eee"><xsl:value-of select="substring-after(physical_item/fulfillment_note, 'heck for')" /></td>
                                                            </xsl:when>
                                                            <xsl:otherwise>
                                                                <td></td>
                                                            </xsl:otherwise>
                                                        </xsl:choose>
                                                    </xsl:if>
                                                </tr>
                                            </xsl:for-each>
                                        </table>
                                    </td>
                                </tr>
                            </xsl:for-each>
                        </xsl:if>
                    </xsl:if>
                    <br/>
                    <xsl:if test="notification_data/organization_fee_list/string">
                        <tr>
                            <td>
                                <strong>@@debt_message@@</strong>
                            </td>
                        </tr>
                        
                        <xsl:for-each select="notification_data/organization_fee_list/string">
                            <tr>
                                <td><xsl:value-of select="."/></td>
                            </tr>
                        </xsl:for-each>
                        
                        <tr>
                            <td>
                                <strong>
                                    @@total@@ <xsl:value-of select="notification_data/total_fee"/>
                                </strong>
                            </td>
                        </tr>
                        
                        <tr>
                            <td>
                                <strong>@@please_pay_message@@</strong>
                                <br/><br/>
                            </td>
                        </tr>
                        
                    </xsl:if>
                    <tr>
                        <td height="40"></td>
                    </tr>
                    <tr>
                        <td>
                            <text>If you have questions, please contact us.</text>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <text>Email: </text><a href="mailto:{$holdingLibEmail}"><xsl:value-of select="$holdingLibEmail"/></a>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <text>Phone: </text><xsl:value-of select="$holdingLibPhone"/>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                    </tr>
                    <tr>
                        <td>
                            <xsl:call-template name="sincerely" /> <!-- mailReason.xsl -->
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <xsl:call-template name="footerTextDUL" /> <!-- footer.xsl -->
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <xsl:call-template name="blueLine" /> <!-- footer.xsl -->
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <xsl:call-template name="dukeLogo" /> <!-- footer.xsl -->
                        </td>
                    </tr>
                </table>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>