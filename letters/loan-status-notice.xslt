<?xml version="1.0" encoding="utf-8"?>

<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    
    <xsl:include href="header.xsl" />
    <xsl:include href="senderReceiver.xsl" />
    <xsl:include href="mailReason.xsl" />
    <xsl:include href="footer.xsl" />
    <xsl:include href="style.xsl" />
    
    <xsl:template match="/">
        <html>
            <xsl:if test="notification_data/languages/string">
                <xsl:attribute name="lang">
                    <xsl:value-of select="notification_data/languages/string"/>
                </xsl:attribute>
            </xsl:if>
            
            <head>
                <title>
                    <xsl:value-of select="notification_data/general_data/subject"/>
                </title>
            </head>
            
            <body>
                <table role="presentation" cellspacing="0" cellpadding="5" style="font-family:Calibri; color:#333333; font-size:12pt; width:100%">
                    <tr>
                        <td height="60">
                            <xsl:call-template name="toWhomIsConcerned" /> <!-- mailReason.xsl -->
                        </td>
                    </tr>
                    <tr>
                        <td height="40" style="font-family:Calibri; color:#333333; font-size:12pt">
                            <xsl:if test="notification_data/message='RECALL_DUEDATE_CHANGE'">
                                @@recall_and_date_change@@
                            </xsl:if>
                            <xsl:if test="notification_data/message='RECALL_ONLY'">
                                <xsl:message terminate="yes" />
                            </xsl:if>
                            <xsl:if test="notification_data/message='DUE_DATE_CHANGE_ONLY'">
                                <xsl:message terminate="yes" />
                            </xsl:if>
                            <xsl:if test="notification_data/message='RECALL_CANCEL_RESTORE_ORIGINAL_DUEDATE'">
                                @@cancel_recall_date_change@@
                            </xsl:if>
                            <xsl:if test="notification_data/message='RECALL_CANCEL_ITEM_RENEWED'">
                                <xsl:message terminate="yes" />
                            </xsl:if>
                            <xsl:if test="notification_data/message='RECALL_CANCEL_NO_CHANGE'">
                                <!-- DON'T SEND THE EMAIL IN THIS INSTANCE -->
                                <xsl:message terminate="yes" />
                            </xsl:if>
                            <br/><br/>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                    </tr>
                    <tr>
                        <td>
                            <table caption="item details" role='presentation'  cellpadding="5">
                                <thead style="background-color:#f5f5f5">
                                    <tr>
                                        <th scope="col">@@title@@</th>
                                        <th scope="col">@@author@@</th>
                                        <th scope="col">Call Number</th>
                                        <th scope="col">@@description@@</th>
                                        <th scope="col">@@old_due_date@@</th>
                                        <th scope="col">@@new_due_date@@</th>
                                        <th scope="col">Barcode</th>
                                    </tr>
                                </thead>
                                
                                <xsl:for-each select="notification_data/item_loans/item_loan">
                                    <tr style="text-align:left">
                                        <td style="border-bottom: 1px solid #eee"><xsl:value-of select="title"/></td>
                                        <td style="border-bottom: 1px solid #eee">
                                            <!-- CHOOSE BEST AUTHOR FIELD, REMOVE AUTHORITY FILE LINK -->
                                            <!-- (Sometimes "author" node is blank when "physical_item/author" is populated) -->
                                            <xsl:choose>
                                                <xsl:when test="author=''">
                                                    <xsl:value-of select="physical_item/author" />
                                                </xsl:when>   
                                                <xsl:otherwise>
                                                    <xsl:choose>
                                                        <xsl:when test="contains(author, 'https://id.')">
                                                            <xsl:value-of select="substring-before(author, ' https://id.')"/>
                                                            <text> </text>
                                                            <xsl:value-of select="substring-after(substring-after(author, 'https://id.'), ' ')"/>
                                                        </xsl:when>
                                                        <xsl:otherwise>
                                                            <xsl:value-of select="author"/>
                                                        </xsl:otherwise>
                                                    </xsl:choose>
                                                </xsl:otherwise>
                                            </xsl:choose>
                                        </td>
                                        <td style="border-bottom: 1px solid #eee"><xsl:value-of select="call_number"/></td>
                                        <td style="border-bottom: 1px solid #eee"><xsl:value-of select="item_description"/></td>
                                        <td style="border-bottom: 1px solid #eee; text-align:center">
                                            <!-- "old_due_date" CONTAINS JUST THE DATE (MM/DD/YY)-->
                                            <xsl:value-of select="old_due_date"/>
                                            <!-- ONLY DISPLAY THE DUE TIME IF IT IS NOT "23:59:00" -->                                                        
                                            <xsl:if test="not(contains(old_due_date_str, '23:59:00'))">
                                                <br />
                                                <xsl:value-of select="substring(old_due_date_str, 12)"/>
                                            </xsl:if>
                                        </td>
                                        <td style="border-bottom: 1px solid #eee; text-align:center; font-weight:bold">
                                            <!-- "due_date" CONTAINS JUST THE DATE (MM/DD/YY)-->
                                            <xsl:value-of select="due_date"/>
                                            <!-- "new_due_date_str" CONTAINS JUST DATE (MM/DD/YY) AND TIME (HH:MM:SS) -->
                                            <!-- ONLY DISPLAY THE DUE TIME IF IT IS NOT "23:59:00" -->                                                        
                                            <xsl:if test="not(contains(new_due_date_str, '23:59:00'))">
                                                <br />
                                                <xsl:value-of select="substring(new_due_date_str, 12)"/>
                                            </xsl:if>
                                        </td>
                                        <td style="border-bottom: 1px solid #eee"><xsl:value-of select="barcode"/></td>
                                    </tr>
                                </xsl:for-each>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td height="20"></td>
                    </tr>
                    <tr>
                        <td>
                            <text>If you have questions, please </text>
                            <a href="https://library.duke.edu/about/contact">contact us</a>.
                        </td>
                    </tr>
                    <tr>
                        <td height="20"></td>
                    </tr>
                    <tr>
                        <td>Thank you,</td>
                    </tr>
                    <tr>
                        <td>
                            <xsl:value-of select="notification_data/organization_unit/name"/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <xsl:call-template name="blueLine" /> <!-- footer.xsl -->
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <xsl:call-template name="footerTextDUL" /> <!-- footer.xsl -->
                        </td>
                    </tr>
                </table>  
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>