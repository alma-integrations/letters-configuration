<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns="http://www.w3.org/2000/svg">
  
  
  <xsl:include href="header.xsl" />
  <xsl:include href="senderReceiver.xsl" />
  <xsl:include href="mailReason.xsl" />
  <xsl:include href="footer.xsl" />
  <xsl:include href="style.xsl" />
  <xsl:include href="recordTitle.xsl" />
  
  
  <xsl:template match="/">
    <html>
      <head>
        <style>
          .messageArea { font-family:arial; color:#333; width:9cm }
          .col_1 { width:26% }
          .col_2 {  }
          .lastName { width:100%; white-space:normal; word-wrap:break-word; overflow-wrap:break-word }
        </style>
      </head>
      <body>
        <div class="messageArea">
          <div class="messageBody">
            <!--
            <xsl:attribute name="style">
              <xsl:call-template name="shelfSlipStyle" />
            </xsl:attribute>
            -->
            
            <!--            
            <xsl:attribute name="style"> 
              <style>
                .col_1 { column-width:15% }
                .col_2 { column-width:85% }
              </style>
            </xsl:attribute>
            -->

            <!-- PATRON NAME -->
            <!-- CHECKS FOR PREFERRED NAMES, USES 'REGULAR' NAMES IF PREFERRED NAMES AREN'T SPECIFIED -->
            <table role="presentation" style="display:block; margin:0.67 0 0.67 0em; font-weight:bold;
              table-layout:fixed; white-space:normal; width:100%; font-size:250%; word-wrap:break-word; overflow-wrap:break-word;">
              <tr>
                <td class="lastName">
                  <xsl:choose>
                    <xsl:when test="boolean(normalize-space(notification_data/user_for_printing/preferred_last_name))">
                      <xsl:value-of select="concat(normalize-space(notification_data/user_for_printing/preferred_last_name), ',')" />
                    </xsl:when>
                    <xsl:otherwise>
                      <xsl:value-of select="concat(normalize-space(notification_data/user_for_printing/last_name), ',')" />
                    </xsl:otherwise>
                  </xsl:choose>
                </td>
              </tr>
              <!-- FIRST NAME A LITTLE SMALLER -->             
              <tr style="font-size:90%">
                <td>
                  <xsl:choose>
                    <xsl:when test="boolean(normalize-space(notification_data/user_for_printing/preferred_first_name))">
                      <xsl:value-of select="normalize-space(notification_data/user_for_printing/preferred_first_name)"/>
                    </xsl:when>
                    <xsl:otherwise>
                      <xsl:value-of select="normalize-space(notification_data/user_for_printing/first_name)" />
                    </xsl:otherwise>
                  </xsl:choose>
                </td>
              </tr>
            </table>
            <!-- BEGIN HOLDS TABLE -->
            <table role="presentation" cellpadding="5" style="display:block; text-align:left; vertical_align:center; font-size:80%;
              table-layout:fixed; margin-left:0em; white-space:normal; overflow:hidden; border-bottom:1px solid #eee; word-break:break-word; width:100%">
              
              <tr align="center" bgcolor="#f5f5f5" >
                <th colspan="2" style="border: 1px solid #333; border-collapse: collapse;">
                  <b>Request details</b>
                </th>
              </tr>
              <tr>
                <td class="col_1"><b>Hold date: </b></td>
                <td class="col_2"><xsl:value-of select="notification_data/general_data/current_date"/></td>
              </tr>
              <tr>
                <td class="col_1"><b>Pickup Sublibrary: </b></td>
                <td class="col_2"><xsl:value-of select="notification_data/request/delivery_address"/></td>
              </tr>
              <tr>
                <td class="col_1"><b>Request Notes: </b></td>
                <td class="col_2"><xsl:value-of select="notification_data/request/note"/></td>
              </tr>
              <tr style="height: 20px" >
              </tr>
              <tr align="center" bgcolor="#dcdcdc">
                <th colspan="2" style="border: 1px solid #333; border-collapse: collapse;">
                  <b>Item details</b>
                </th>
              </tr>
              <tr>
                <td class="col_1"><b>Owning Sublibrary: </b></td>
                <td class="col_2"><xsl:value-of select="notification_data/phys_item_display/owning_library_details/name"/></td>
              </tr>
              <tr>
                <td class="col_1"><b>Collection: </b></td>
                <td class="col_2"><xsl:value-of select="notification_data/phys_item_display/location_name"/></td>
              </tr>
              <tr>
                <td class="col_1"><b>Call Number: </b></td>
                <td class="col_2"><xsl:value-of select="notification_data/phys_item_display/call_number"/></td>
              </tr>
              <tr>
                <td class="col_1"><b>Description: </b></td>
                <td class="col_2"><xsl:value-of select="notification_data/phys_item_display/issue_level_description"/></td>
              </tr>
              <tr>
                <td class="col_1"><b>Barcode: </b></td>
                <td class="col_2"><xsl:value-of select="notification_data/phys_item_display/barcode"/></td>
              </tr>
              <tr>
                <td class="col_1"><b>Title: </b></td>
                <td class="col_2"><xsl:value-of select="notification_data/phys_item_display/title"/></td>
              </tr>
              <tr>
                <td class="col_1"><b>Author: </b></td>
                <td class="col_2"><xsl:value-of select="notification_data/phys_item_display/author"/></td>
              </tr>
              <tr style="height: 20px" >
              </tr>
              
              <!-- END HOLDS TABLE -->
              
              <!-- BEGIN DETAILED BIB SUMMARY-->
              <tr>
                <td colspan="2">
                  <br />
                  <b>Full bibliographic info: </b>
                </td>
              </tr>
              <tr>
                <td colspan="2">
                  
                  <div class="bibSummary" style="display:block; font-size:65%; text-align:left; margin:1 0.5 0 0.5em; word-break:break-word">
                    <xsl:value-of select="concat(notification_data/request/record_display_section/author, ' ')"/> 
                    <xsl:value-of select="concat(notification_data/request/record_display_section/title_abcnph, ' ')"/> 
                    <xsl:value-of select="concat(notification_data/phys_item_display/imprint, ' ')"/> 
                    <xsl:value-of select="concat(notification_data/phys_item_display/publication_place, ' ')"/> 
                    <xsl:value-of select="concat(notification_data/phys_item_display/publisher, ' ')"/>
                    <xsl:value-of select="concat(notification_data/phys_item_display/publication_date, ' ')"/> 
                    <xsl:value-of select="normalize-space(notification_data/request/record_display_section/format)"/>
                  </div>
                </td>
              </tr>    
            </table>
            <!-- END DETAILED BIB SUMMARY-->
            
            <!-- BEGIN BARCODE SECTION -->
            <table cellspacing="0" cellpadding="5" border="0" style="display:block; align:left; text-align:center; vertical_align:center; font-size:70%; width:100%; white-space:normal; border-bottom:1px solid #eee;">
              <!--
              <xsl:if  test="notification_data/request/selected_inventory_type='ITEM'" >
                <tr style="height: 20px" >
                </tr>
                <tr>
                  <td><b>@@note_item_specified_request@@.</b></td>
                </tr>
              </xsl:if>
              <xsl:if  test="notification_data/request/manual_description != ''" >
                <tr>
                  <td><b>@@please_note@@: </b>@@manual_description_note@@ - <xsl:value-of select="notification_data/request/manual_description"/></td>
                </tr>
              </xsl:if>
              -->
              
              <tr>
                <td style="vertical-align:center; border-bottom:1px solid #eee">
                  <b>@@item_barcode@@: </b>
                  <img src="cid:item_id_barcode.png" alt="Item Barcode" style="height:50%" />
                </td> 
              </tr>
              
              <xsl:if  test="notification_data/external_id != ''" >
                <tr>
                  <td><b>@@external_id@@: </b><xsl:value-of select="notification_data/external_id"/></td>
                </tr>
              </xsl:if>
              <!-- END BARCODE SECTION -->
              
              <!-- BEGIN LETTER NAME SECTION -->
              <tr>
                <td><br /></td>
              </tr>
              <tr>
                <td style="text-align:center">
                  <text>Duke Libraries - </text>
                  <xsl:value-of select="notification_data/general_data/letter_name"/>
                  <br />
                  <xsl:value-of select="notification_data/general_data/current_date"/>
                  <text> - </text>
                  <xsl:value-of select="notification_data/general_data/current_time"/>
                </td>
              </tr>
            </table>
            <!-- END LETTER NAME SECTION -->
            
          </div>
        </div>
      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>