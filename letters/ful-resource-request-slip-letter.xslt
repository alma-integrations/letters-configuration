<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns="http://www.w3.org/2000/svg">
  
  
  <xsl:include href="header.xsl" />
  <xsl:include href="senderReceiver.xsl" />
  <xsl:include href="mailReason.xsl" />
  <xsl:include href="footer.xsl" />
  <xsl:include href="style.xsl" />
  <xsl:include href="recordTitle.xsl" />
  
  <!-- GENERIC REPLACE-STRING TEMPLATE -->
  <!-- XSLT 1.0 HAS NO REPLACE FUNCTION LIKE THIS -->
  <xsl:template name="replace-string">
    <xsl:param name="text"/>
    <xsl:param name="replace"/>
    <xsl:param name="with"/>
    <xsl:choose>
      <xsl:when test="contains($text,$replace)">
        <xsl:value-of select="substring-before($text,$replace)"/>
        <xsl:value-of select="$with"/>
        <xsl:call-template name="replace-string">
          <xsl:with-param name="text"
            select="substring-after($text,$replace)"/>
          <xsl:with-param name="replace" select="$replace"/>
          <xsl:with-param name="with" select="$with"/>
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$text"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  
  
  <xsl:template match="/">
    <html>
      <head>
        <style>
          .messageArea { font-family:arial; color:#333; width:9cm }
          .col_1 { width:26%; text-align:left }
          .col_2 { }
        </style>
      </head>
      <body>
        <div class="messageArea">
          <div class="messageBody">
            
            <!-- BEGIN TABLE WITH PATRON NAME -->
            <table role="presentation" style="display:block; margin:0.67 0 0.67 0em; font-weight:bold;
              table-layout:fixed; white-space:normal; overflow:hidden; width:100%; font-size:100%; word-wrap:break-word; overflow-wrap:break-word;">
              <tr>
                
                <td style="font-size:170%; padding:0px">
                  <xsl:choose>
                    <xsl:when test="boolean(normalize-space(notification_data/user_for_printing/preferred_last_name))">
                      <xsl:value-of select="concat(normalize-space(notification_data/user_for_printing/preferred_last_name), ',')" />
                    </xsl:when>
                    <xsl:otherwise>
                      <xsl:value-of select="concat(normalize-space(notification_data/user_for_printing/last_name), ',')" />
                    </xsl:otherwise>
                  </xsl:choose>
                </td>
                
              </tr>
              <!-- FIRST NAME SLIGHTLY SMALLER -->             
              <tr>
                <td style="font-size:140%; padding:0px">
                  <xsl:choose>
                    <xsl:when test="boolean(normalize-space(notification_data/user_for_printing/preferred_first_name))">
                      <xsl:value-of select="normalize-space(notification_data/user_for_printing/preferred_first_name)"/>
                    </xsl:when>
                    <xsl:otherwise>
                      <xsl:value-of select="normalize-space(notification_data/user_for_printing/first_name)" />
                    </xsl:otherwise>
                  </xsl:choose>
                </td>
                <td></td>
              </tr>
              <!-- ADD PATRON GROUP IF FACULTY MEMBER -->
              <tr>
                <td style="font-size:120%; padding:2px">
                  <xsl:if test="/notification_data/user_for_printing/user_group='01'">
                    Patron group: Faculty
                  </xsl:if>
                  <xsl:if test="/notification_data/user_for_printing/user_group='02'">
                    Patron group: Fuqua Faculty
                  </xsl:if>
                  <xsl:if test="/notification_data/user_for_printing/user_group='03'">
                    Patron group: Law Faculty
                  </xsl:if>
                  <xsl:if test="/notification_data/user_for_printing/user_group='04'">
                    Patron group: MCL Faculty
                  </xsl:if>
                </td>
              </tr>
            </table>
            <!-- BEGIN HOLDS TABLE -->
            <table role="presentation" cellpadding="5" style="display:block; text-align:left; font-size:80%;
              table-layout:fixed; margin-left:0em; white-space:normal; overflow:hidden; border-bottom:1px solid #eee; word-break:break-word; width:100%">
              
              <tr align="center" bgcolor="#f5f5f5" >
                <th colspan="2" style="border: 1px solid #333; border-collapse: collapse;">
                  <b>Request details</b>
                </th>
              </tr>
              <tr>
                <td class="col_1"><b>Hold date: </b></td>
                <td class="col_2"><xsl:value-of select="notification_data/general_data/current_date"/></td>
              </tr>
              <tr>
                <td class="col_1"><b>Pickup at: </b></td>
                <td class="col_2"><xsl:value-of select="notification_data/request/delivery_address"/></td>
              </tr>
              <tr>
                <td class="col_1"><b>Request Notes: </b></td>
                <td class="col_2"><xsl:value-of select="notification_data/request/note"/></td>
              </tr>
              <tr style="height: 20px" >
              </tr>
              <tr align="center" bgcolor="#dcdcdc">
                <th colspan="2" style="border: 1px solid #333; border-collapse: collapse;">
                  <b>Item details</b>
                </th>
              </tr>
              <tr>
                <td class="col_1"><b>Sublibrary: </b></td>
                <td class="col_2"><xsl:value-of select="notification_data/phys_item_display/owning_library_details/name"/></td>
              </tr>
              <tr>
                <td class="col_1"><b>Collection: </b></td>
                <td class="col_2"><xsl:value-of select="notification_data/phys_item_display/location_name"/></td>
              </tr>
              <tr>
                <td class="col_1"><b>Call Number: </b></td>
                <td class="col_2"><xsl:value-of select="notification_data/phys_item_display/call_number"/></td>
              </tr>
              <tr>
                <td class="col_1"><b>Description: </b></td>
                <td class="col_2"><xsl:value-of select="notification_data/phys_item_display/issue_level_description"/></td>
              </tr>
              <tr>
                <td class="col_1"><b>Barcode: </b></td>
                <td class="col_2"><xsl:value-of select="notification_data/phys_item_display/barcode"/></td>
              </tr>
              <tr>
                <td class="col_1"><b>Title: </b></td>
                <td class="col_2"><xsl:value-of select="notification_data/phys_item_display/title"/></td>
              </tr>
              <tr>
                <td class="col_1"><b>Author: </b></td>
                <td class="col_2"><xsl:value-of select="notification_data/phys_item_display/author"/></td>
              </tr>
              <tr style="height: 20px" >
              </tr>
              
              <!-- END HOLDS TABLE -->
              
              <!-- BEGIN DETAILED BIB SUMMARY-->
              <tr>
                <td colspan="2" style="padding-bottom:0px; padding-top:0px">
                  <br />
                  <b>Full bibliographic info: </b>
                </td>
              </tr>
              <tr>
                <td colspan="2" style="padding:0 5 5 5px">
                  
                  <div style="display:block; font-size:65%; text-align:left; word-break:break-word">
                    <xsl:value-of select="concat(notification_data/request/record_display_section/author, ' ')"/> 
                    <xsl:value-of select="concat(notification_data/request/record_display_section/title_abcnph, ' ')"/> 
                    <xsl:choose>
                      <xsl:when test="notification_data/phys_item_display/imprint != ''">
                        <xsl:value-of select="concat(notification_data/phys_item_display/imprint, ' ')"/>
                      </xsl:when>
                      <xsl:otherwise>
                        <xsl:value-of select="concat(notification_data/phys_item_display/publication_place, ' ')"/> 
                        <xsl:value-of select="concat(notification_data/phys_item_display/publisher, ' ')"/>
                        <xsl:value-of select="concat(notification_data/phys_item_display/publication_date, ' ')"/>
                      </xsl:otherwise>
                    </xsl:choose>
                    <!-- REMOVE DOUBLE SEMICOLONS FROM 'FORMAT' | ***TEMPLATE DEFINED IN THIS SHEET*** -->
                    <xsl:variable name="bibSummaryFormat">
                      <xsl:call-template name="replace-string">
                        <xsl:with-param name="text" select="normalize-space(notification_data/request/record_display_section/format)"/>
                        <xsl:with-param name="replace" select="';;'" />
                        <xsl:with-param name="with" select="';'"/>
                      </xsl:call-template>
                    </xsl:variable>
                    <!-- ALSO REMOVE COLON-SEMICOLON PAIRS WHILE WE'RE AT IT -->
                    <xsl:choose>
                      <xsl:when test="contains($bibSummaryFormat, ':;')">
                        <xsl:call-template name="replace-string">
                          <xsl:with-param name="text" select="$bibSummaryFormat"/>
                          <xsl:with-param name="replace" select="':;'" />
                          <xsl:with-param name="with" select="': '"/>
                        </xsl:call-template>
                      </xsl:when>
                      <xsl:otherwise>
                        <xsl:value-of select="$bibSummaryFormat"/>
                      </xsl:otherwise>
                    </xsl:choose>
                    
                  </div>
                </td>
              </tr>    
            </table>
            <!-- END DETAILED BIB SUMMARY-->
            
            <!-- BEGIN BARCODE SECTION -->
            <table cellspacing="0" cellpadding="5" border="0" style="display:block; text-align:middle; vertical_align:center; font-size:70%; width:100%; white-space:normal; border-bottom:1px solid #eee;">
              <!-- THIS PART WAS IN THE DEFAULT EX LIBRIS LETTER. -->
              <!-- WE MAY NOT NEED IT, BUT DON'T DELETE IT YET -->
              <!--
                <xsl:if  test="notification_data/request/selected_inventory_type='ITEM'" >
                <tr style="height: 20px" >
                </tr>
                <tr>
                <td><b>@@note_item_specified_request@@.</b></td>
                </tr>
                </xsl:if>
                <xsl:if  test="notification_data/request/manual_description != ''" >
                <tr>
                <td><b>@@please_note@@: </b>@@manual_description_note@@ - <xsl:value-of select="notification_data/request/manual_description"/></td>
                </tr>
                </xsl:if>
              -->
              <tbody style="display:block" align="center">
                <tr>
                  <td style="text-align:center">
                    <b>@@item_barcode@@: </b>
                    <img src="cid:item_id_barcode.png" alt="Item Barcode" style="height:60%" />
                  </td> 
                </tr>
                
                <xsl:if  test="notification_data/external_id != ''" >
                  <tr>
                    <td><b>@@external_id@@: </b><xsl:value-of select="notification_data/external_id"/></td>
                  </tr>
                </xsl:if>
              </tbody>
            </table>
            <!-- END BARCODE SECTION -->
            
            <!-- BEGIN LETTER NAME SECTION -->
            <table cellpadding="5" border="0" style="display:block; text-align:center; font-size:70%; white-space:normal; border-bottom:1px solid #eee; width:100%; color:#333333">
              <tbody style="display:block" align="center">
                <tr>
                  <td><br /></td>
                </tr>
                <tr>
                  <td style="text-align:center" align="center">
                    <text>Duke Libraries - </text>
                    <xsl:value-of select="notification_data/general_data/letter_name"/>
                    <br />
                    <xsl:value-of select="notification_data/general_data/current_date"/>
                    <text> - </text>
                    <xsl:value-of select="notification_data/general_data/current_time"/>
                  </td>
                </tr>
              </tbody>
            </table>
            <!-- END LETTER NAME SECTION -->
            
          </div>
        </div>
      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>