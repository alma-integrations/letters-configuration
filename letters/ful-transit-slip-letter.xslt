<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:include href="header.xsl" />
    <xsl:include href="senderReceiver.xsl" />
    <xsl:include href="mailReason.xsl" />
    <xsl:include href="footer.xsl" />
    <xsl:include href="style.xsl" />
    <xsl:include href="recordTitle.xsl" />
    
    <!-- GENERIC REPLACE-STRING TEMPLATE -->
    <!-- XSLT 1.0 HAS NO REPLACE FUNCTION LIKE THIS -->
    <xsl:template name="replace-string">
        <xsl:param name="text"/>
        <xsl:param name="replace"/>
        <xsl:param name="with"/>
        <xsl:choose>
            <xsl:when test="contains($text,$replace)">
                <xsl:value-of select="substring-before($text,$replace)"/>
                <xsl:value-of select="$with"/>
                <xsl:call-template name="replace-string">
                    <xsl:with-param name="text"
                        select="substring-after($text,$replace)"/>
                    <xsl:with-param name="replace" select="$replace"/>
                    <xsl:with-param name="with" select="$with"/>
                </xsl:call-template>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="$text"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    <xsl:template match="/">
        <html>

            <!--do not generate letter if it's a patron request-->
            <xsl:if test="/notification_data/request/work_flow_entity/assigned_unit_type='CIRCULATION_DESK'">
                <xsl:if test="/notification_data/request/request_type='PATRON_PHYSICAL'">
                    <xsl:message terminate="yes">do not print</xsl:message>
                </xsl:if>
                <xsl:if test="/notification_data/request/request_type='TRANSIT_FOR_RESHELVING'">
                    <xsl:message terminate="yes">do not print</xsl:message>
                </xsl:if>
            </xsl:if>

            <xsl:if test="notification_data/languages/string">
                <xsl:attribute name="lang">
                    <xsl:value-of select="notification_data/languages/string"/>
                </xsl:attribute>
            </xsl:if>
            
            <head>
                <title>
                    <xsl:value-of select="notification_data/general_data/subject"/>
                </title>
                <style>
                    .messageArea { font-family:arial; color:#333; width:9cm }
                </style>
                
                <xsl:call-template name="generalStyle" />
            </head>
            <body>
                
                <xsl:attribute name="style">
                    <xsl:call-template name="bodyStyleCss" />
                </xsl:attribute>
                
                
                <div class="messageArea">
                    <div class="messageBody">
                        <table role='presentation' cellspacing="0" cellpadding="5" border="0" style="display:block; table-layout:fixed; white-space:normal; overflow:hidden; width:9cm; word-wrap:break-word; overflow-wrap:break-word;">
                            <tr>
                                <td><strong>Please deliver to: </strong>
                                    <xsl:value-of select="notification_data/request/delivery_address" /></td>
                            </tr>
                            <xsl:choose>
                                <!-- SHOW SPECIFIC LANGUAGE AND REQUEST BARCODE IF IT'S IN TRANSIT FOR A PATRON REQUEST -->
                                <xsl:when test="notification_data/user_for_printing != ''">
                                    <tr style="text-align:center">
                                        <td>
                                            <strong>*** RUSH for Patron Request ***</strong>                                        
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <strong>@@request_id@@: </strong>
                                            <img src="cid:request_id_barcode.png"
                                                alt="Request Barcode" />
                                        </td>
                                    </tr>
                                </xsl:when>
                                <xsl:otherwise>
                                    <tr>
                                        <td></td>
                                    </tr>   
                                </xsl:otherwise>
                            </xsl:choose>
                            <tr>
                                <td>
                                    <strong>@@item_barcode@@: </strong>
                                    <img src="cid:item_id_barcode.png"
                                        alt="Item Barcode" />
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                            </tr>
                                    <td>@@we_are_transferring_item_below@@.</td>
                            <tr>
                                <td></td>
                            </tr>
                            <tr>
                                <td><strong>@@from@@: </strong>
                                    <xsl:value-of select="notification_data/request/assigned_unit_name" /></td>
                            </tr>
                            <tr>
                                <td><strong>@@to@@: </strong>
                                    <xsl:value-of select="notification_data/request/delivery_address" /></td>
                            </tr>
                            
                            <!--
                                <tr>
                                <td><strong>@@transfer_date@@: </strong>
                                <xsl:value-of select="notification_data/request/create_date" /></td>
                                </tr>
                                <tr>
                                <td><strong>@@transfer_time@@: </strong>
                                <xsl:value-of select="notification_data/request/create_time" /></td>
                                </tr>
                            -->
                            
                            <xsl:if test="notification_data/request/material_type_display">
                                <tr>
                                    <td><strong>@@material_type@@: </strong>
                                        <xsl:value-of select="notification_data/request/material_type_display" /></td>
                                </tr>
                            </xsl:if>
                            <!-- HIDE PATRON INFORMATION -->
                            <!--
                                <xsl:if test="notification_data/user_for_printing/note">
                                <tr>
                                <td>
                                <strong>@@user_note@@:</strong>
                                </td>
                                </tr>
                                <tr>
                                <td>
                                <xsl:value-of select="notification_data/user_for_printing/note" />
                                </td>
                                </tr>
                                </xsl:if>
                            -->
                            <xsl:if test="notification_data/request/system_notes != ''">
                                <tr>
                                    <td>
                                        <strong>@@system_notes@@:</strong>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <xsl:value-of select="notification_data/request/system_notes" />
                                    </td>
                                </tr>
                            </xsl:if>
                            <xsl:if test="notification_data/request/note != ''">
                                <tr>
                                    <td>
                                        <strong>@@request_note@@:</strong>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <xsl:value-of select="notification_data/request/note" />
                                    </td>
                                </tr>
                            </xsl:if>
                            
                            
                            <xsl:if test="notification_data/user_for_printing/name">
                                <!-- HIDE PATRON INFORMATION -->
                                <!--
                                    <tr>
                                    <td>
                                    <strong>@@requested_for@@:</strong>
                                    </td>
                                    </tr>
                                    <tr>
                                    <td>
                                    <xsl:value-of select="notification_data/user_for_printing/name" />
                                    </td>
                                    </tr>
                                    <xsl:if test="notification_data/user_for_printing/email">
                                    <tr>
                                    <td><strong>@@email@@: </strong>
                                    <xsl:value-of select="notification_data/user_for_printing/email" /></td>
                                    </tr>
                                    </xsl:if>
                                    <xsl:if test="notification_data/user_for_printing/phone">
                                    <tr>
                                    <td><strong>@@tel@@: </strong>
                                    <xsl:value-of select="notification_data/user_for_printing/phone" /></td>
                                    </tr>
                                    </xsl:if>
                                -->
                                
                                <tr>
                                    <td><strong>@@request_date@@: </strong>
                                        <xsl:value-of select="notification_data/request/create_date" /></td>
                                </tr>
                                <xsl:if test="notification_data/request/lastInterestDate">
                                    <tr>
                                        <td><strong>@@expiration_date@@: </strong>
                                            <xsl:value-of select="notification_data/request/lastInterestDate" /></td>
                                    </tr>
                                </xsl:if>
                            </xsl:if>
                            
                            <!--
                                <tr>
                                <td><xsl:call-template name="recordTitle" /></td>
                                </tr>
                            -->
                            <xsl:if test="notification_data/phys_item_display/owning_library_name">
                                <tr>
                                    <td><strong>@@owning_library@@: </strong>
                                        <xsl:value-of select="notification_data/phys_item_display/owning_library_name" /></td>
                                </tr>
                            </xsl:if>
                            <tr>
                                <td><strong>Call Number: </strong>
                                    <xsl:value-of select="notification_data/phys_item_display/call_number" /></td>
                            </tr>
                            <tr>
                                <td><strong>Description: </strong>
                                    <xsl:value-of select="notification_data/phys_item_display/issue_level_description" /></td>
                            </tr>
                            <tr>
                                <td><strong>Barcode: </strong>
                                    <xsl:value-of select="notification_data/phys_item_display/barcode" /></td>
                            </tr>
                            
                            <!-- BEGIN DETAILED BIB SUMMARY-->
                            <tr>
                                <td colspan="2" style="padding-bottom:0px; padding-top:0px">
                                    <br />
                                    <b>Full bibliographic info: </b>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" style="padding:0 5 5 5px">
                                    
                                    <div style="display:block; font-size:65%; text-align:left; word-break:break-word">
                                        <xsl:value-of select="concat(notification_data/request/record_display_section/author, ' ')"/> 
                                        <xsl:value-of select="concat(notification_data/request/record_display_section/title_abcnph, ' ')"/> 
                                        <xsl:choose>
                                            <xsl:when test="notification_data/phys_item_display/imprint != ''">
                                                <xsl:value-of select="concat(notification_data/phys_item_display/imprint, ' ')"/>
                                            </xsl:when>
                                            <xsl:otherwise>
                                                <xsl:value-of select="concat(notification_data/phys_item_display/publication_place, ' ')"/> 
                                                <xsl:value-of select="concat(notification_data/phys_item_display/publisher, ' ')"/>
                                                <xsl:value-of select="concat(notification_data/phys_item_display/publication_date, ' ')"/>
                                            </xsl:otherwise>
                                        </xsl:choose>
                                        <!-- REMOVE DOUBLE SEMICOLONS FROM 'FORMAT' | ***TEMPLATE DEFINED IN THIS SHEET*** -->
                                        <xsl:variable name="bibSummaryFormat">
                                            <xsl:call-template name="replace-string">
                                                <xsl:with-param name="text" select="normalize-space(notification_data/request/record_display_section/format)"/>
                                                <xsl:with-param name="replace" select="';;'" />
                                                <xsl:with-param name="with" select="';'"/>
                                            </xsl:call-template>
                                        </xsl:variable>
                                        <!-- ALSO REMOVE COLON-SEMICOLON PAIRS WHILE WE'RE AT IT -->
                                        <xsl:choose>
                                            <xsl:when test="contains($bibSummaryFormat, ':;')">
                                                <xsl:call-template name="replace-string">
                                                    <xsl:with-param name="text" select="$bibSummaryFormat"/>
                                                    <xsl:with-param name="replace" select="':;'" />
                                                    <xsl:with-param name="with" select="': '"/>
                                                </xsl:call-template>
                                            </xsl:when>
                                            <xsl:otherwise>
                                                <xsl:value-of select="$bibSummaryFormat"/>
                                            </xsl:otherwise>
                                        </xsl:choose>
                                        
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                            </tr>
                            <tr>
                                <td>
                                    <strong>@@print_date@@: </strong>
                                    <span style="font-size:80%">
                                        <xsl:value-of select="notification_data/request/create_date" />
                                        <text> - </text>
                                        <xsl:value-of select="notification_data/request/create_time"/>
                                    </span>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>