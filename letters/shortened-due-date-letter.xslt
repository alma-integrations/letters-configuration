<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    
    <xsl:include href="header.xsl" />
    <xsl:include href="senderReceiver.xsl" />
    <xsl:include href="mailReason.xsl" />
    <xsl:include href="footer.xsl" />
    <xsl:include href="style.xsl" />
    <xsl:include href="recordTitle.xsl" />
    
    <xsl:template match="/">
        <html>
            <xsl:if test="notification_data/languages/string">
                <xsl:attribute name="lang">
                    <xsl:value-of select="notification_data/languages/string"/>
                </xsl:attribute>
            </xsl:if>
            
            <head>
                <title>
                    <xsl:value-of select="notification_data/general_data/subject"/>
                </title>
            </head>
            
            <!-- NOTE THAT THIS LETTER IS ONLY CONFIGURED FOR SHORTENED DUE DATE DUE TO ADDITIONAL REQUESTS IN THE QUEUE. -->
            <!-- NOT FOR SHORTENED DUE DATE BECAUSE OF AN APPROACHING PATRON EXPIRATION DATE -->
            <!-- IF WE WANT TO IMPLEMENT THAT, IT WILL NEED A CONDITIONAL BASED ON THE VALUE IN "notification_data/item_loan/shortened_due_date_reason" -->
            
            <body>
                <table role='presentation' cellspacing="0" cellpadding="5" border="0" style="font-family:Calibri; color:#333333; font-size:12pt; width:100%">
                    <tr>
                        <td height="60">
                            <!-- LETTER GREETING -->
                            <xsl:call-template name="toWhomIsConcerned" /> <!-- mailReason.xsl -->
                        </td>
                    </tr>
                    <tr>
                        <td height="40">
                            <text>This library item you checked out has a shorter loan period than usual because there are additional patrons waiting to borrow it. </text>
                            <text>Unfortunately, this means you will not be able to renew it. </text>
                        </td>
                    </tr>
                    <tr>
                        <td height="40">
                            <text>Please return it to any </text>
                            <a href="https://library.duke.edu/libraries">Duke Libraries location</a>
                            <text> by the due date listed below to avoid late fees.</text>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                    </tr>
                    <!-- BEGIN TABLE OF ITEM INFORMATION -->
                    <tr>
                        <td>
                            <table caption="item details" cellpadding="5">
                                <thead style="background-color:#f5f5f5; text-align:center">
                                    <tr>
                                        <th scope="col">Title</th>
                                        <th scope="col">Author</th>
                                        <th scope="col">Call Number</th>
                                        <th scope="col">Description</th>
                                        <th scope="col">Due Date</th>
                                        <th scope="col">Barcode</th>
                                    </tr>
                                </thead>
                                <xsl:for-each select="notification_data/item_loan">
                                    <tr style="text-align:left">
                                        <td style="border-bottom: 1px solid #eee"><xsl:value-of select="title"/></td>
                                        <td style="border-bottom: 1px solid #eee">
                                            <!-- CHOOSE BEST AUTHOR FIELD, REMOVE AUTHORITY FILE LINK -->
                                            <!-- (Sometimes "author" node is blank when "physical_item/author" is populated) -->
                                            <xsl:choose>
                                                <xsl:when test="author=''">
                                                    <xsl:value-of select="physical_item/author" />
                                                </xsl:when>
                                                <xsl:otherwise>
                                                    <xsl:choose>
                                                        <xsl:when test="contains(author, 'https://id.')">
                                                            <xsl:value-of select="substring-before(author, ' https://id.')"/>
                                                            <text> </text>
                                                            <xsl:value-of select="substring-after(substring-after(author, 'https://id.'), ' ')"/>
                                                        </xsl:when>
                                                        <xsl:otherwise>
                                                            <xsl:value-of select="author"/>
                                                        </xsl:otherwise>
                                                    </xsl:choose>
                                                </xsl:otherwise>
                                            </xsl:choose>
                                        </td>
                                        <td style="border-bottom: 1px solid #eee"><xsl:value-of select="call_number"/></td>
                                        <td style="border-bottom: 1px solid #eee"><xsl:value-of select="description"/></td>
                                        <td style="border-bottom: 1px solid #eee; text-align:center; font-weight:bold">
                                            <!-- "due_date" CONTAINS JUST THE DATE (MM/DD/YY)-->
                                            <xsl:value-of select="due_date"/>
                                            <!-- "new_due_date_str" CONTAINS JUST DATE (MM/DD/YY) AND TIME (HH:MM:SS) -->
                                            <!-- ONLY DISPLAY THE DUE TIME IF IT IS NOT "23:59:00" -->                                                        
                                            <xsl:if test="not(contains(new_due_date_str, '23:59:00'))">
                                                <br />
                                                <xsl:value-of select="substring(new_due_date_str, 12)"/>
                                            </xsl:if>
                                        </td>
                                        <td style="border-bottom: 1px solid #eee"><xsl:value-of select="barcode"/></td>
                                    </tr>
                                </xsl:for-each>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td height="20"></td>
                    </tr>
                    <tr>
                        <td>
                            <text>If you have questions, please </text>
                            <a href="https://library.duke.edu/about/contact">contact us</a>.
                        </td>
                    </tr>
                    <tr>
                        <td height="20"></td>
                    </tr>
                    <tr>
                        <td>Thank you,</td>
                    </tr>
                    <tr>
                        <td>
                            <xsl:value-of select="notification_data/organization_unit/name"/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <xsl:call-template name="blueLine" /> <!-- footer.xsl -->
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <xsl:call-template name="footerTextDUL" /> <!-- footer.xsl -->
                        </td>
                    </tr>
                </table>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>