<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:include href="header.xsl" />
    <xsl:include href="senderReceiver.xsl" />
    <xsl:include href="mailReason.xsl" />
    <xsl:include href="footer.xsl" />
    <xsl:include href="style.xsl" />
    <xsl:include href="recordTitle.xsl" />
    <xsl:template match="/">
        <html>
            <xsl:if test="notification_data/languages/string">
                <xsl:attribute name="lang">
                    <xsl:value-of select="notification_data/languages/string"/>
                </xsl:attribute>
            </xsl:if>
            
            <head>
                <title>
                    <xsl:value-of select="notification_data/general_data/subject"/>
                </title>
                
            </head>
            <body>
                <table role='presentation' cellspacing="0" cellpadding="5" border="0" style="font-family:Calibri; color:#333333; font-size:12pt; width:100%">
                    <tr>
                        <td height="60">
                            <xsl:call-template name="toWhomIsConcerned" /> <!-- mailReason.xsl -->
                        </td>
                    </tr>        
                    <tr>
                        <td>@@we_cancel_y_req_of@@</td>
                    </tr>
                    <tr>
                        <td></td>
                    </tr>
                    <!-- BEGIN TABLE OF ITEM INFORMATION -->
                    <tr>
                        <td>
                            <table caption="item details" role='presentation' cellpadding="5">
                                <thead style="background-color:#f5f5f5; text-align:center">
                                    <tr>
                                        <th scope="col">Title</th>
                                        <th scope="col">Author</th>
                                        <th scope="col">Call Number</th>
                                        <th scope="col">Description</th>
                                        <th scope="col">Barcode</th>
                                    </tr>
                                </thead>
                                <tr style="text-align:left">
                                    <td style="border-bottom: 1px solid #eee"><xsl:value-of select="notification_data/phys_item_display/title"/></td>
                                    <td style="border-bottom: 1px solid #eee"><xsl:value-of select="notification_data/phys_item_display/author" /></td>
                                    <td style="border-bottom: 1px solid #eee"><xsl:value-of select="notification_data/phys_item_display/call_number" /></td>
                                    <td style="border-bottom: 1px solid #eee"><xsl:value-of select="notification_data/phys_item_display/description" /></td>
                                    <td style="border-bottom: 1px solid #eee"><xsl:value-of select="notification_data/phys_item_display/barcode" /></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <!-- ALL THIS STUFF WAS HERE AND COMMENTED OUT IN THE DEFAULT LETTER -->
                    <!-- <xsl:if test="notification_data/metadata/title != ''">
                        <tr>
                        <td>
                        <strong>@@title@@: </strong>
                        <xsl:value-of select="notification_data/metadata/title" />
                        </td>
                        </tr>
                        </xsl:if>
                        <xsl:if test="notification_data/metadata/journal_title != ''">
                        <tr>
                        <td>
                        <strong> @@journal_title@@: </strong>
                        <xsl:value-of select="notification_data/metadata/journal_title" />
                        </td>
                        </tr>
                        </xsl:if>
                        <xsl:if test="notification_data/metadata/author != ''">
                        <tr>
                        <td>
                        <strong> @@author@@: </strong>
                        <xsl:value-of select="notification_data/metadata/author" />
                        </td>
                        </tr>
                        </xsl:if>
                        <xsl:if test="notification_data/metadata/author_initials != ''">
                        <tr>
                        <td>
                        <strong>@@author_initials@@: </strong>
                        <xsl:value-of select="notification_data/metadata/author_initials" />
                        </td>
                        </tr>
                        </xsl:if>
                        <xsl:if test="notification_data/metadata/edition != ''">
                        <tr>
                        <td>
                        <strong> @@edition@@: </strong>
                        <xsl:value-of select="notification_data/metadata/edition" />
                        </td>
                        </tr>
                        </xsl:if>
                        <xsl:if test="notification_data/metadata/identifier != ''">
                        <tr>
                        <td>
                        <strong>@@identifier@@: </strong>
                        <xsl:value-of select="notification_data/metadata/identifier" />
                        </td>
                        </tr>
                        </xsl:if>
                        <xsl:if test="notification_data/metadata/lccn != ''">
                        <tr>
                        <td>
                        <strong> @@lccn@@: </strong>
                        <xsl:value-of select="notification_data/metadata/lccn" />
                        </td>
                        </tr>
                        </xsl:if>
                        <xsl:if test="notification_data/metadata/issn != ''">
                        <tr>
                        <td>
                        <strong>@@issn@@: </strong>
                        <xsl:value-of select="notification_data/metadata/issn" />
                        </td>
                        </tr>
                        </xsl:if>
                        <xsl:if test="notification_data/metadata/isbn != ''">
                        <tr>
                        <td>
                        <strong> @@isbn@@: </strong>
                        <xsl:value-of select="notification_data/metadata/isbn" />
                        </td>
                        </tr>
                        </xsl:if>
                        <xsl:if test="notification_data/metadata/oclc_nr != ''">
                        <tr>
                        <td>
                        <strong> @@oclc_nr@@: </strong>
                        <xsl:value-of select="notification_data/metadata/oclc_nr" />
                        </td>
                        </tr>
                        </xsl:if>
                        <xsl:if test="notification_data/metadata/doi != ''">
                        <tr>
                        <td>
                        <strong>@@doi@@: </strong>
                        <xsl:value-of select="notification_data/metadata/doi" />
                        </td>
                        </tr>
                        </xsl:if>
                        <xsl:if test="notification_data/metadata/pmid != ''">
                        <tr>
                        <td>
                        <strong> @@pmid@@: </strong>
                        <xsl:value-of select="notification_data/metadata/pmid" />
                        </td>
                        </tr>
                        </xsl:if>
                        <xsl:if test="notification_data/metadata/publisher != ''">
                        <tr>
                        <td>
                        <strong> @@publisher@@: </strong>
                        <xsl:value-of select="notification_data/metadata/publisher" />
                        </td>
                        </tr>
                        </xsl:if>
                        <xsl:if test="notification_data/metadata/publication_date != ''">
                        <tr>
                        <td>
                        <strong>@@publication_date@@: </strong>
                        <xsl:value-of select="notification_data/metadata/publication_date" />
                        </td>
                        </tr>
                        </xsl:if>
                        <xsl:if test="notification_data/metadata/place_of_publication != ''">
                        <tr>
                        <td>
                        <strong> @@place_of_publication@@: </strong>
                        <xsl:value-of select="notification_data/metadata/place_of_publication" />
                        </td>
                        </tr>
                        </xsl:if>
                        <xsl:if test="notification_data/metadata/additional_person_name != ''">
                        <tr>
                        <td>
                        <strong> @@additional_person_name@@: </strong>
                        <xsl:value-of select="notification_data/metadata/additional_person_name" />
                        </td>
                        </tr>
                        </xsl:if>
                        <xsl:if test="notification_data/metadata/source != ''">
                        <tr>
                        <td>
                        <strong>@@source@@: </strong>
                        <xsl:value-of select="notification_data/metadata/source" />
                        </td>
                        </tr>
                        </xsl:if>
                        <xsl:if test="notification_data/metadata/series_title_number != ''">
                        <tr>
                        <td>
                        <strong> @@series_title_number@@: </strong>
                        <xsl:value-of select="notification_data/metadata/series_title_number" />
                        </td>
                        </tr>
                        </xsl:if>
                        <xsl:if test="notification_data/metadata/call_number != ''">
                        <tr>
                        <td>
                        <strong> @@call_number@@: </strong>
                        <xsl:value-of select="notification_data/metadata/call_number" />
                        </td>
                        </tr>
                        </xsl:if>
                        <xsl:if test="notification_data/metadata/volume != ''">
                        <tr>
                        <td>
                        <strong>@@volume@@: </strong>
                        <xsl:value-of select="notification_data/metadata/volume" />
                        </td>
                        </tr>
                        </xsl:if>
                        <xsl:if test="notification_data/metadata/part != ''">
                        <tr>
                        <td>
                        <strong> @@part@@: </strong>
                        <xsl:value-of select="notification_data/metadata/part" />
                        </td>
                        </tr>
                        </xsl:if>
                        <xsl:if test="notification_data/metadata/chapter != ''">
                        <tr>
                        <td>
                        <strong> @@chapter@@: </strong>
                        <xsl:value-of select="notification_data/metadata/chapter" />
                        </td>
                        </tr>
                        </xsl:if>
                        <xsl:if test="notification_data/metadata/pages != ''">
                        <tr>
                        <td>
                        <strong>@@pages@@: </strong>
                        <xsl:value-of select="notification_data/metadata/pages" />
                        </td>
                        </tr>
                        </xsl:if>
                        <xsl:if test="notification_data/metadata/start_page != ''">
                        <tr>
                        <td>
                        <strong> @@start_page@@: </strong>
                        <xsl:value-of select="notification_data/metadata/start_page" />
                        </td>
                        </tr>
                        </xsl:if>
                        <xsl:if test="notification_data/metadata/end_pagee != ''">
                        <tr>
                        <td>
                        <strong> @@end_page@@: </strong>
                        <xsl:value-of select="notification_data/metadata/end_page" />
                        </td>
                        </tr>
                        </xsl:if>
                        <xsl:if test="notification_data/metadata/issue != ''">
                        <tr>
                        <td>
                        <strong>@@issue@@: </strong>
                        <xsl:value-of select="notification_data/metadata/issue" />
                        </td>
                        </tr>
                        </xsl:if>
                        <xsl:if test="notification_data/metadata/note != ''">
                        <tr>
                        <td>
                        <strong> @@note@@: </strong>
                        <xsl:value-of select="notification_data/metadata/note" />
                        </td>
                        </tr>
                        </xsl:if> -->
                    
                    <!-- THIS WAS NOT COMMENTED OUT IN THE DEFAULT LETTER, BUT WE PROBABLY DON'T WANT IT -->
                    <!--
                        <xsl:if test="notification_data/request/start_time != ''">
                        <tr>
                        <td>
                        <strong> @@start_time@@: </strong>
                        <xsl:value-of select="notification_data/booking_start_time_str" />
                        </td>
                        </tr>
                        </xsl:if>
                        <xsl:if test="notification_data/request/end_time != ''">
                        <tr>
                        <td>
                        <strong> @@end_time@@: </strong>
                        <xsl:value-of select="notification_data/booking_end_time_str" />
                        </td>
                        </tr>
                        </xsl:if>
                        <xsl:if test="notification_data/request/note != ''">
                        <tr>
                        <td>
                        <strong> @@request_note@@: </strong>
                        <xsl:value-of select="notification_data/request/note" />
                        </td>
                        </tr>
                        </xsl:if>
                    -->
                    
                    <tr>
                        <td></td>
                    </tr>
                    <tr>
                        <td>
                            <strong> @@reason_deleting_request@@: </strong>
                            <xsl:value-of select="notification_data/request/status_note_display" />
                        </td>
                    </tr>
                    <xsl:if test="notification_data/request/cancel_reason != ''">
                        <tr>
                            <td>
                                <strong> @@request_cancellation_note@@: </strong>
                                <xsl:value-of select="notification_data/request/cancel_reason" />
                            </td>
                        </tr>
                    </xsl:if>
                    <tr>
                        <td></td>
                    </tr>
                    <tr>
                        <td>
                            <text>If you have questions, please </text>
                            <a href="https://library.duke.edu/about/contact">contact us</a>.
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table role='presentation' cellpadding="0">
                                <tr><td>Thank you,</td></tr>
                                <tr><td><xsl:value-of select="notification_data/organization_unit/name" /></td></tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <xsl:call-template name="blueLine" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <xsl:call-template name="footerTextDUL" />
                        </td>
                    </tr>
                </table>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>