<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    
    
    <xsl:include href="header.xsl" />
    <xsl:include href="senderReceiver.xsl" />
    <xsl:include href="mailReason.xsl" />
    <xsl:include href="footer.xsl" />
    <xsl:include href="style.xsl" />
    <xsl:include href="recordTitle.xsl" />
    
    <xsl:template match="/">
        <html>
            <xsl:if test="notification_data/languages/string">
                <xsl:attribute name="lang">
                    <xsl:value-of select="notification_data/languages/string"/>
                </xsl:attribute>
            </xsl:if>
            <head>
                <title>
                    <xsl:value-of select="notification_data/general_data/subject"/>
                </title>
            </head>
            <body>
                <table role='presentation' cellspacing="0" cellpadding="5" border="0" style="font-family:Calibri; color:#333333; font-size:12pt; width:100%">
                    <tr>
                        <td height="60">
                            <xsl:call-template name="toWhomIsConcerned" /> <!-- mailReason.xsl -->
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <text>The items you requested are still available for pick-up at the </text>
                            <xsl:value-of select="notification_data/requests_by_library/library_requests_for_display/requests/request_for_display/request/delivery_address"/>
                            <text>service desk.</text>
                        </td>
                    </tr>
                    <tr>
                        <td height="40">
                            <text>We'll hold them for you until the dates listed below.</text>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                    </tr>
                    <!-- BEGIN TABLE OF ITEM INFORMATION -->
                    <tr>
                        <td>
                            <table caption="item details" role='presentation' cellpadding="5">
                                <thead style="background-color:#f5f5f5; text-align:center">
                                    <tr>
                                        <th scope="col">Title</th>
                                        <th scope="col">Author</th>
                                        <th scope="col">Pickup by</th>
                                        <th scope="col">Pickup location</th>
                                    </tr>
                                </thead>
                                <xsl:for-each select="notification_data/requests_by_library/library_requests_for_display/requests/request_for_display/phys_item_display">
                                    <tr style="text-align:left">
                                        <td style="border-bottom: 1px solid #eee"><xsl:value-of select="title"/></td>
                                        <td style="border-bottom: 1px solid #eee"><xsl:value-of select="author" /></td>
                                        <td style="border-bottom: 1px solid #eee; text-align:center; font-weight:bold; word-break:keep-all"><xsl:value-of select="//request/work_flow_entity/expiration_date"/></td>
                                        <td style="border-bottom: 1px solid #eee; text-align:center"><xsl:value-of select="//request/assigned_unit_name"/></td>
                                    </tr>
                                </xsl:for-each>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td height="20"></td>
                    </tr>
                    <tr>
                        <td>
                            <text>If you have questions, please </text>
                            <a href="https://library.duke.edu/about/contact">contact us</a>.
                        </td>
                    </tr>
                    <tr>
                        <td height="20"></td>
                    </tr>
                    <tr>
                        <td>Thank you,</td>
                    </tr>
                    <tr>
                        <td>
                            <xsl:value-of select="notification_data/organization_unit/name"/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <xsl:call-template name="blueLine" /> <!-- footer.xsl -->
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <xsl:call-template name="footerTextDUL" /> <!-- footer.xsl -->
                        </td>
                    </tr>
                </table>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>