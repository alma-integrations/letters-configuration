<?xml version="1.0" encoding="utf-8"?>

<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    
    <xsl:include href="header.xsl" />
    <xsl:include href="mailReason.xsl" />
    <xsl:include href="senderReceiver.xsl" />
    <xsl:include href="footer.xsl" />
    <xsl:include href="style.xsl" />
    
    <xsl:template match="/">
        <html>
            <xsl:if test="notification_data/languages/string">
                <xsl:attribute name="lang">
                    <xsl:value-of select="notification_data/languages/string"/>
                </xsl:attribute>
            </xsl:if>
            
            <head>
                <title>
                    <xsl:value-of select="notification_data/general_data/subject"/>
                </title>
            </head>
            <body>
                
                <table role="presentation" cellspacing="0" cellpadding="5" style="font-family:Source Sans Pro, Calibri, Candara, Arial, sans-serif; color:#333333; font-size:12pt; width:100%">
                    <tr>
                        <td height="60">
                            <xsl:call-template name="toWhomIsConcerned" /> <!-- mailReason.xsl -->
                        </td>
                    </tr>
                    <tr>
                        <td>
                            @@inform_you_item_below@@
                            @@decalred_as_lost@@.
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                    </tr>
                    <tr>
                        <td>
                            <xsl:for-each select="notification_data/item_loan">
                                <table caption="item details" role='presentation' cellpadding="5">
                                    <thead style="background-color:#f5f5f5">
                                        <tr>
                                            <th scope="col">@@lost_item@@</th>
                                            <th scope="col">@@by@@</th>
                                            <th scope="col">@@call_number@@</th>
                                            <th scope="col">@@description@@</th>
                                            <th scope="col">@@due_date@@</th>
                                            <th scope="col">@@barcode@@</th>
                                        </tr>
                                    </thead>
                                    <tr>
                                        <td style="border-bottom: 1px solid #eee"><xsl:value-of select="title"/></td>
                                        <td style="border-bottom: 1px solid #eee">
                                            <!-- CHOOSE BEST AUTHOR FIELD, REMOVE AUTHORITY FILE LINK -->
                                            <!-- (Sometimes "author" node is blank when "physical_item/author" is populated) -->
                                            <xsl:choose>
                                                <xsl:when test="author=''">
                                                    <xsl:value-of select="physical_item/author" />
                                                </xsl:when>   
                                                <xsl:otherwise>
                                                    <xsl:choose>
                                                        <xsl:when test="contains(author, 'https://id.')">
                                                            <xsl:value-of select="substring-before(author, ' https://id.')"/>
                                                            <text> </text>
                                                            <xsl:value-of select="substring-after(substring-after(author, 'https://id.'), ' ')"/>
                                                        </xsl:when>
                                                        <xsl:otherwise>
                                                            <xsl:value-of select="author"/>
                                                        </xsl:otherwise>
                                                    </xsl:choose>
                                                </xsl:otherwise>
                                            </xsl:choose>
                                        </td>
                                        <td style="border-bottom: 1px solid #eee"><xsl:value-of select="physical_item/call_number"/></td>
                                        <td style="border-bottom: 1px solid #eee"><xsl:value-of select="description"/></td>
                                        <td style="border-bottom: 1px solid #eee; text-align:center"><xsl:value-of select="due_date"/></td>
                                        <td style="border-bottom: 1px solid #eee; text-align:center"><xsl:value-of select="barcode"/></td>
                                    </tr>
                                    <tr>
                                        <td height="15"></td>
                                    </tr>
                                </table>
                            </xsl:for-each>
                            <tr>
                                <td>
                                    @@charged_with_fines_fees@@ 
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                            </tr>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table caption="fee details" role='presentation'  cellpadding="5">
                                <thead style="background-color:#f5f5f5">
                                    <tr>
                                        <th scope="col">@@fee_type@@</th>
                                        <th scope="col">@@fee_amount@@</th>
                                    </tr>
                                </thead>
                                <xsl:for-each select="notification_data/fines_fees_list/user_fines_fees">
                                    <tr>
                                        <td style="border-bottom: 1px solid #eee"><xsl:value-of select="fine_fee_type_display"/></td>
                                        <td style="border-bottom: 1px solid #eee"><xsl:value-of select="concat('$', fine_fee_ammount/normalized_sum)"/></td>
                                    </tr>
                                </xsl:for-each>
                                <!-- "TOTAL" ROW: CALCULATES TOTAL OF ALL FEES IN TABLE -->
                                <tr>
                                    <th scope="row" style="text-align:right; background-color:#f5f5f5">TOTAL:</th>
                                    <td style="text-align:left; background-color:#f5f5f5; font-weight:bold" >
                                        <xsl:value-of select="concat('$', format-number(sum(//notification_data/fines_fees_list/user_fines_fees/fine_fee_ammount/normalized_sum[. != '']), '#.00'))"/> 
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td height="20"></td>
                    </tr>
                    <tr>
                        <td>
                            <text>If you have questions, or if you think an item has already been returned, please </text>
                            <a href="https://library.duke.edu/about/contact">contact us</a>.
                        </td>
                    </tr>
                    <tr>
                        <td height="20"></td>
                    </tr>
                    <tr>
                        <td>Thank you,</td>
                    </tr>
                    <tr>
                        <td>
                            <xsl:value-of select="notification_data/organization_unit/name"/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <xsl:call-template name="blueLine" /> <!-- footer.xsl -->
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <xsl:call-template name="footerTextDUL" /> <!-- footer.xsl -->
                        </td>
                    </tr>
                </table>
            </body>
        </html>
    </xsl:template>
    
</xsl:stylesheet>