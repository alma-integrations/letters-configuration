<?xml version="1.0" encoding="utf-8"?>

<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    
    <xsl:include href="header.xsl" />
    <xsl:include href="senderReceiver.xsl" />
    <xsl:include href="mailReason.xsl" />
    <xsl:include href="footer.xsl" />
    <xsl:include href="style.xsl" />
    
    <xsl:template match="/">
        <html>
            <xsl:if test="notification_data/languages/string">
                <xsl:attribute name="lang">
                    <xsl:value-of select="notification_data/languages/string"/>
                </xsl:attribute>
            </xsl:if>
            
            <head>
                <title>
                    <xsl:value-of select="notification_data/general_data/subject"/>
                </title>
                
                <!-- <xsl:call-template name="generalStyle" /> -->
            </head>
            
            <body>
                <!-- BEGIN MESSAGE -->
                <table role='presentation' cellspacing="0" cellpadding="5" border="0" style="font-family:Calibri; color:#333333; font-size:12pt; width:auto">
                    <tr>
                        <td height="60">
                            <!-- LETTER GREETING -->
                            <!-- NOTE: THIS XML MIGHT NOT CONTAIN PREFERRED NAMES. MIGHT HAVE TO ALTER THIS -->
                            <xsl:call-template name="toWhomIsConcerned" /> <!-- mailReason.xsl -->
                        </td>
                    </tr>
                    <tr>
                        <td height="40">
                            <text>One or more items you have checked out are now very overdue and are considered lost. Please return them to a </text>
                            <a href="https://library.duke.edu/libraries">Duke Libraries location</a><text> as soon as possible or </text>
                            <a href="https://library.duke.edu/about/contact">contact us</a><text> to pay for or provide replacements for the items.</text>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                    </tr>
                    <!-- BEGIN TABLE OF ITEM INFORMATION -->
                    <tr>
                        <td>
                            <xsl:for-each select="notification_data/loans_by_library/library_loans_for_display">
                                <tr>
                                    <td>
                                        <table caption="item details" role='presentation' cellpadding="5" border-collapse="collapse">
                                            <thead style="background-color:#f5f5f5; text-align:center">
                                                <tr>
                                                    <th scope="col" width="25%">@@lost_item@@</th>
                                                    <th scope="col">Author</th>
                                                    <th scope="col">@@barcode@@</th>
                                                    <th scope="col">@@due_date@@</th>
                                                    <td style="background-color:#ffffff; padding:0; border:0; border-spacing:0px">
                                                        <table role="presentation" cellpadding="0" style="border-spacing:2px 0px" width="100%">
                                                            <thead style="background-color:#ffffff; text-align:center">
                                                                <tr>
                                                                    <th scope="col" width="55%" style="padding:10; background-color:#f5f5f5; line-height:30px">Reason</th>
                                                                    <th scope="col" style="padding:10; background-color:#f5f5f5; line-height:30px">@@charged_with_fines_fees@@</th>
                                                                </tr>
                                                            </thead>
                                                            
                                                        </table>
                                                    </td>
                                                </tr>
                                            </thead>
                                            <xsl:for-each select="item_loans/overdue_and_lost_loan_notification_display">
                                                <tr>
                                                    <td style="border-bottom: 1px solid #eee"><xsl:value-of select="item_loan/title"/></td>
                                                    <td style="border-bottom: 1px solid #eee">
                                                        <!-- CHOOSE BEST AUTHOR FIELD, REMOVE AUTHORITY FILE LINK IF PRESENT -->
                                                        <!-- (Sometimes "author" node is blank when "physical_item/author" is populated) -->
                                                        <xsl:choose>
                                                            <xsl:when test="item_loan/author=''">
                                                                <xsl:value-of select="//physical_item/author" />
                                                            </xsl:when>   
                                                            <xsl:otherwise>
                                                                <xsl:choose>
                                                                    <xsl:when test="contains(item_loan/author, 'https://id.')">
                                                                        <xsl:value-of select="substring-before(item_loan/author, ' https://id.')"/>
                                                                        <text> </text>
                                                                        <xsl:value-of select="substring-after(substring-after(item_loan/author, 'https://id.'), ' ')"/>
                                                                    </xsl:when>
                                                                    <xsl:otherwise>
                                                                        <xsl:value-of select="item_loan/author"/>
                                                                    </xsl:otherwise>
                                                                </xsl:choose>
                                                            </xsl:otherwise>
                                                        </xsl:choose>
                                                    </td>
                                                    <td style="border-bottom: 1px solid #eee; text-align:center"><xsl:value-of select="item_loan/barcode"/></td>
                                                    <td style="border-bottom: 1px solid #eee; text-align:center" word-break="keep-all"><xsl:value-of select="item_loan/due_date"/></td>
                                                    <td style="border-bottom: 1px solid #eee">
                                                        <table role="presentation" width="100%">
                                                            <xsl:for-each select="fines_fees_list/user_fines_fees">
                                                                <tr>
                                                                    <xsl:choose>
                                                                        <xsl:when test="position() = 1">
                                                                            <td width="55%"><xsl:value-of select="fine_fee_type_display"/></td>
                                                                            <td style="text-align:left; padding:5"><xsl:value-of select="concat('$', fine_fee_ammount/sum)"/></td>
                                                                        </xsl:when>
                                                                        <xsl:otherwise>
                                                                            <td style="border-top: 1px solid #eee" width="55%"><xsl:value-of select="fine_fee_type_display"/></td>
                                                                            <td style="border-top: 1px solid #eee; text-align:left; padding:5" text-align="right"><xsl:value-of select="concat('$', fine_fee_ammount/sum)"/></td>
                                                                        </xsl:otherwise>
                                                                    </xsl:choose>
                                                                </tr>
                                                            </xsl:for-each>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </xsl:for-each>
                                            <!-- "TOTAL" ROW: CALCULATES TOTAL OF ALL FEES IN TABLE -->
                                            <tr>
                                                <td colspan="4" style="background-color:#f5f5f5"></td>
                                                <td style="background-color:#ffffff; padding:0; border:0">
                                                    <table role="presentation" cellpadding="10" style="border-spacing:2px 0px" width="100%">
                                                        <thead style="background-color:#ffffff; text-align:center; height:100%; border-spacing:0px">
                                                            <tr line-height="50px">
                                                                <th scope="row" colspan="3" style="text-align:right; width:55%; background-color:#f5f5f5">TOTAL:</th>
                                                                <td style="text-align:left; background-color:#f5f5f5; font-weight:bold" >
                                                                    <xsl:value-of select="concat('$', format-number(sum(//item_loans/overdue_and_lost_loan_notification_display/fines_fees_list/user_fines_fees/fine_fee_ammount/sum[. != '']), '#.00'))"/> 
                                                                </td>
                                                            </tr>
                                                        </thead>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <br/>
                            </xsl:for-each>
                            <!-- NOT SEEING THIS NODE IN THE XML, COMMENTING OUT FOR NOW | JLK -->
                            <!--
                                <xsl:if test="notification_data/overdue_notification_fee_amount/sum !=''">
                                <tr>
                                <td>
                                <strong>@@overdue_notification_fee@@</strong>
                                <xsl:value-of select="notification_data/overdue_notification_fee_amount/normalized_sum"/>&#160;<xsl:value-of select="notification_data/overdue_notification_fee_amount/currency"/>&#160;<xsl:value-of select="ff"/>
                                </td>
                                </tr>
                                </xsl:if>
                            -->
                            <tr>
                                <td height="20"></td>
                            </tr>
                            <tr>
                                <td>
                                    <text>If you have questions, or if you think an item has already been returned, please </text>
                                    <a href="https://library.duke.edu/about/contact">contact us</a>.
                                </td>
                            </tr>
                            <tr>
                                <td height="20"></td>
                            </tr>
                            <tr>
                                <td>Thank you,</td>
                            </tr>
                            <tr>
                                <td>
                                    <xsl:value-of select="notification_data/organization_unit/name"/>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <xsl:call-template name="blueLine" /> <!-- footer.xsl -->
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <xsl:call-template name="footerTextDUL" /> <!-- footer.xsl -->
                                </td>
                            </tr>
                        </td>
                    </tr>
                </table>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>