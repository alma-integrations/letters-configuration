<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    
    <xsl:include href="header.xsl" />
    <xsl:include href="senderReceiver.xsl" />
    <xsl:include href="mailReason.xsl" />
    <xsl:include href="footer.xsl" />
    <xsl:include href="style.xsl" />
    <xsl:include href="recordTitle.xsl" />
    
    <xsl:template name="proxyName">
        <!-- TO GET PREFERRED NAMES OF PROXY BORROWER -->
        <!-- USE PREFERRED NAMES IF AVAILABLE, DEFAULT TO REGULAR NAMES -->
        <xsl:variable name="first" select="concat(normalize-space(notification_data/borrower/preferred_first_name), ' ')" />
        <xsl:choose>
            <xsl:when test="boolean(normalize-space(notification_data/borrower/preferred_first_name))">
                <xsl:value-of select="$first"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="concat(normalize-space(notification_data/borrower/first_name), ' ')" />
            </xsl:otherwise>
        </xsl:choose>
        <xsl:variable name="last" select="normalize-space(notification_data/borrower/preferred_last_name)" />
        <xsl:choose>
            <xsl:when test="boolean(normalize-space(notification_data/borrower/preferred_last_name))">      
                <xsl:value-of select="$last" />
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="normalize-space(notification_data/borrower/last_name)" />
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    <xsl:template match="/">
        <html>
            <xsl:if test="notification_data/languages/string">
                <xsl:attribute name="lang">
                    <xsl:value-of select="notification_data/languages/string"/>
                </xsl:attribute>
            </xsl:if>
            
            <head>
                <title>
                    <xsl:value-of select="notification_data/general_data/subject"/>
                </title>
                
                <!-- <xsl:call-template name="generalStyle" /> -->
            </head>
            <body>
                
                <table role='presentation' cellspacing="0" cellpadding="5" border="0" style="font-family:Calibri; color:#333333; font-size:12pt; width:100%">
                    <tr>
                        <td height="60">
                            <!-- LETTER GREETING -->
                            <xsl:call-template name="toWhomIsConcerned" /> <!-- mailReason.xsl -->
                        </td>
                    </tr>        
                    <tr>
                        <td>                            
                            @@picked_up_by@@, <xsl:call-template name="proxyName"/>, <!-- TEMPLATE DEFINED IN THIS SHEET -->
                            @@the_item@@
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                    </tr>
                    <tr>
                        <td>
                            <table caption="item details" cellpadding="5">
                                <thead style="background-color:#f5f5f5; text-align:center">
                                    <tr>
                                        <th scope="col">Title</th>
                                        <th scope="col">Author</th>
                                        <!-- THESE NODES ARE IN THE XML BUT NOT POPULATED WITH LOCAL DATA AT THIS TIME -->
                                        <xsl:if test="notification_data/phys_item_display/call_number != ''">
                                            <th scope="col">Call Number</th>
                                        </xsl:if>
                                        <xsl:if test="notification_data/phys_item_display/description != ''">
                                            <th scope="col">Description</th>
                                        </xsl:if>
                                        <th scope="col">Due Date</th>
                                        <th scope="col">Barcode</th>
                                    </tr>
                                </thead>
                                
                                <xsl:for-each select="notification_data/phys_item_display">
                                    <tr style="text-align:left">
                                        <td style="border-bottom: 1px solid #eee"><xsl:value-of select="title"/></td>
                                        <td style="border-bottom: 1px solid #eee">
                                            <!-- REMOVE AUTHORITY FILE LINK -->
                                            <xsl:choose>
                                                <xsl:when test="contains(author, 'https://id.')">
                                                    <xsl:value-of select="substring-before(author, ' https://id.')"/>
                                                    <text> </text>
                                                    <xsl:value-of select="substring-after(substring-after(author, 'https://id.'), ' ')"/>
                                                </xsl:when>
                                                <xsl:otherwise>
                                                    <xsl:value-of select="author"/>
                                                </xsl:otherwise>
                                            </xsl:choose>
                                        </td>
                                        <xsl:if test="call_number != ''">
                                            <td><xsl:value-of select="call_number"/></td>
                                        </xsl:if>
                                        <xsl:if test="description != ''">
                                            <td><xsl:value-of select="description"/></td>
                                        </xsl:if>
                                        <td style="border-bottom: 1px solid #eee; text-align:center">
                                            <xsl:value-of select="substring(//due_date, 1, 10)"/>
                                            <!-- ONLY DISPLAY THE DUE TIME IF IT IS NOT "23:59:00" -->
                                            <!-- "due_date" IN THIS XML BEHAVES LIKE "due_date_str" IN OTHER LETTERS -->
                                            <xsl:if test="not(contains(//due_date, '23:59:00'))">
                                                <br />
                                                <xsl:value-of select="substring(//due_date, 12)"/>
                                            </xsl:if>
                                        </td>
                                        <td style="border-bottom: 1px solid #eee">
                                            <xsl:value-of select="barcode"/>
                                        </td>    
                                    </tr>
                                    <tr>
                                        <td></td>
                                    </tr>
                                </xsl:for-each>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td height="20"></td>
                    </tr>
                    <tr>
                        <td>
                            <text>If you have questions, please </text>
                            <a href="https://library.duke.edu/about/contact">contact us</a>.
                        </td>
                    </tr>
                    <tr>
                        <td height="20"></td>
                    </tr>
                    <tr>
                        <td>Thank you,</td>
                    </tr>
                    <tr>
                        <td>
                            <xsl:value-of select="notification_data/organization_unit/name"/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <xsl:call-template name="blueLine" /> <!-- footer.xsl -->
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <xsl:call-template name="footerTextDUL" /> <!-- footer.xsl -->
                        </td>
                    </tr>
                </table>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>