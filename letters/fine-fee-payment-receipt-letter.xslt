<?xml version="1.0" encoding="utf-8"?>

<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    
    <xsl:include href="header.xsl" />
    <xsl:include href="senderReceiver.xsl" />
    <xsl:include href="mailReason.xsl" />
    <xsl:include href="footer.xsl" />
    <xsl:include href="style.xsl" />
    
    <xsl:template match="/">
        <html>
            <xsl:if test="notification_data/languages/string">
                <xsl:attribute name="lang">
                    <xsl:value-of select="notification_data/languages/string"/>
                </xsl:attribute>
            </xsl:if>
            
            <head>
                <title>
                    <xsl:value-of select="notification_data/general_data/subject"/>
                </title>
            </head>
            
            <body>
                <table role='presentation' cellspacing="0" cellpadding="5" border="0" style="font-family:Calibri; color:#333333; font-size:12pt; width:100%">
                    <tr>
                        <td height="60">
                            <!-- LETTER GREETING -->
                            <!-- NO PREFERRED NAMES IN THIS XML -->
                            <xsl:call-template name="toWhomIsConcerned" /> <!-- mailReason.xsl -->
                        </td>
                    </tr>
                    <tr>
                        <td height="40">
                            <text>Thank you for your payment of </text>
                            <xsl:value-of select="concat('$', notification_data/total_amount_paid)"/>.
                        </td>
                    </tr>
                    <xsl:if  test="notification_data/transaction_id != ''" >
                        <tr>
                            <td height="40">
                                <text>Your Transaction ID number is </text>
                                <xsl:value-of select="notification_data/transaction_id"/>.
                            </td>
                        </tr>
                    </xsl:if>
                    <tr>
                        <td height="20">
                        </td>
                    </tr>
                    <!-- WHAT IN THE WORLD IS THIS? - JLK -->
                    <!-- (IN DEFAULT LETTER) -->
                    <!-- 
                        <xsl:for-each select="notification_data/labels_list">
                        <table role='presentation'  cellspacing="0" cellpadding="5" border="0">
                        <tr>
                        <td><xsl:value-of select="letter.fineFeePaymentReceiptLetter.message"/></td>
                        </tr>
                        </table>
                        </xsl:for-each>
                    -->
                    <tr>
                        <td>
                            <table caption="payment details" cellpadding="5">
                                <thead style="background-color:#f5f5f5; text-align:center">
                                    <tr>
                                        <th scope="col">@@fee_type@@</th>
                                        <th scope="col">@@payment_date@@</th>
                                        <th scope="col">@@paid_amount@@</th>
                                        <th scope="col">@@payment_method@@</th>
                                        <th scope="col">@@note@@</th>
                                    </tr>
                                </thead>
                                <xsl:for-each select="notification_data/user_fines_fees_list/user_fines_fees">
                                    <tr style="text-align:left">
                                        <td style="border-bottom: 1px solid #eee"><xsl:value-of select="fine_fee_type_display"/></td>
                                        <td style="border-bottom: 1px solid #eee; text-align:center"><xsl:value-of select="create_date"/></td>
                                        <td style="border-bottom: 1px solid #eee"><xsl:value-of select="concat('$', fines_fee_transactions/fines_fee_transaction/transaction_amount_display)"/></td>
                                        <td style="border-bottom: 1px solid #eee; text-align:center"><xsl:value-of select="fines_fee_transactions/fines_fee_transaction/transaction_method"/></td>
                                        <td style="border-bottom: 1px solid #eee"><xsl:value-of select="fines_fee_transactions/fines_fee_transaction/transaction_note"/></td>
                                    </tr>
                                </xsl:for-each>
                                <!-- "TOTAL" ROW: CALCULATES TOTAL OF ALL FEES IN TABLE -->
                                <!-- SHOULD BE EQUAL TO "notification_data/total_amount_paid" -->
                                <tr>
                                    <th scope="row" colspan="2" style="text-align:right; background-color:#f5f5f5">TOTAL:</th>
                                    <td style="text-align:left; background-color:#f5f5f5; font-weight:bold" >
                                        <xsl:value-of select="concat('$', format-number(sum(//user_fines_fees_list/user_fines_fees/fines_fee_transactions/fines_fee_transaction/transaction_amount_display[. != '']), '#.00'))"/> 
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td height="20"></td>
                    </tr>
                    <tr>
                        <td>
                            <text>If you have questions, please </text>
                            <a href="https://library.duke.edu/about/contact">contact us</a>.
                        </td>
                    </tr>
                    <tr>
                        <td height="20"></td>
                    </tr>
                    <tr>
                        <td>Thank you,</td>
                    </tr>
                    <tr>
                        <td>
                            <xsl:value-of select="notification_data/organization_unit/name"/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <xsl:call-template name="blueLine" /> <!-- footer.xsl -->
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <xsl:call-template name="footerTextDUL" /> <!-- footer.xsl -->
                        </td>
                    </tr>
                </table>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>